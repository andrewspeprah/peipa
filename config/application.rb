require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Transpage
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.autoload_paths << Rails.root.join('lib')
    config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
    config.autoload_paths += Dir[Rails.root.join('app', 'api', '*')]

    config.assets.paths << "#{Rails.root}/app/assets/stylesheets/"
    config.assets.precompile += %w( bootstrap.css )

    # Filter these parameters
    config.filter_parameters += [:password, :pin_code, :account_id, :customer_id, :agent_id,
                                 :account_no, :sap_code, :Message, :id, :recipient_no,
                                 :customer_no, :first_name, :last_name, :invoice]

    # ======================================================
    #  Mailer settings
    # ======================================================
    config.action_mailer.default_url_options = { host: ENV['mailer_host'] }

    if ENV['mailer_service'] == 'smtp'
      config.action_mailer.delivery_method = :smtp
      config.action_mailer.raise_delivery_errors = true
      config.action_mailer.smtp_settings = {
          address: ENV['mailer_smtp_address'],
          domain:  ENV['mailer_smtp_domain'],
          port:    ENV['mailer_smtp_port'],

          user_name: ENV['mailer_smtp_email'],
          password:  ENV['mailer_smtp_password'],

          authentication: ENV['mailer_smtp_authentication'],
          enable_starttls_auto: true
      }
    else
      config.action_mailer.delivery_method = :test
      config.action_mailer.raise_delivery_errors = false
    end
    # ======================================================
    #  ~ Mailer settings
    # ======================================================

    # Raven
    # ------------------------------------------------------
    if Rails.env.production?
      Raven.configure do |config|
        config.dsn = 'https://c928e11e88074e4097e37fd916f2a39c:293b2586523d4a92899483fee7a71e7b@sentry.io/234963'
      end
    end
  end
end
