# require 'resque/server'
Rails.application.routes.draw do
  devise_for :users, :path => ""


  # Of course, you need to substitute your application name here, a block
  # like this probably already exists.
  # mount Resque::Server.new, at: "/resque"
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  # Api for grape
  mount API => '/'

  resources :users, :path => "", only: :index do
  end

  resources :presses do
  	member do 
      put :toggle
      get :inventory
      get :metrics
  	end
  end

  resources :rannars do
    collection do
      get "delivery"
    end
    member do
    end
  end

  resources :orders do
    member do
      get :download
      post :status
    end
  end

  resources :countries
  resources :vouchers do
    collection do
      get   :print
      post  :get_pdf
    end
  end
  resources :customers
  
	resources :transactions do
		collection do
			get :export_csv
		end
	end
	resources :reports
	resources :settings do
		collection do
			put :password
		end
	end

  resources :dashboards
  resources :charges
  resources :services do
    member do
      namespace :services do
        resources :papers
      end
    end
  end

  root "dashboards#index"
end
