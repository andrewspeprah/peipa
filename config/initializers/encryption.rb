Encryption.config do |e|
    e.key = ENV['encryption_key']
    e.iv = ENV['encryption_iv']
    e.cipher = ENV['encryption_algo'] # if you're feeling adventurous
end