# configure s3
$amazon_s3 = Aws::S3::Resource.new(
  region: 'us-east-1',
  access_key_id: ENV['S3_ACCESS_KEY_ID'],
  secret_access_key: ENV['S3_SECRET_ACCESS_KEY']
)
