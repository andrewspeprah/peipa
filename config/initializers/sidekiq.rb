require 'sidekiq'
require 'sidekiq-status'

Sidekiq.configure_client do |config|
  config.client_middleware do |chain|
    chain.add Sidekiq::Status::ClientMiddleware
  end
  if Rails.env.production?
    config.redis = {  :namespace => 'Sidekiq', 
                      :url => 'redis://127.0.0.1:6379/0',
                      :password => ENV['REDIS_PASSWORD'] }
  else
    config.redis = {  :namespace => 'Sidekiq', 
                      :url => 'redis://127.0.0.1:6379/0'}
  end
end

Sidekiq.configure_server do |config|
  config.server_middleware do |chain|
    chain.add Sidekiq::Status::ServerMiddleware, expiration: 30.minutes # default
  end
  if Rails.env.production?
    config.redis = {  :namespace => 'Sidekiq', 
                      :url => 'redis://127.0.0.1:6379/0',
                      :password => ENV['REDIS_PASSWORD'] }
  else
    config.redis = {  :namespace => 'Sidekiq', 
                      :url => 'redis://127.0.0.1:6379/0'}
  end
end

