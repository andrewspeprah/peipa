Money.default_currency = Money::Currency.new("GHS")

Money.add_rate("GHS", "USD", 2.00)
Money.add_rate("USD", "GHS", 1.00)

Money.add_rate("GHS", "UGX", 843.98)
Money.add_rate("UGX", "GHS", 0.0012)

Money.add_rate("GHS", "NGN", 58.51)
Money.add_rate("NGN", "GHS", 0.017)

Money.add_rate("GHS", "XAF", 175.72)
Money.add_rate("XAF", "GHS", 0.01)

Money.add_rate("GHS", "XOF", 175.72)
Money.add_rate("XOF", "GHS", 0.01)


curr = {
  :priority            => 1,
  :iso_code            => "GHS",
  :iso_numeric         => "840",
  :name                => "Ghana cedis",
  :subunit             => "Pesewas",
  :subunit_to_unit     => 10000,
  :decimal_mark        => ".",
  :thousands_separator => ","
}

Money::Currency.register(curr)