namespace :server do
  desc 'Restarts resque on the server'
  task :restart do
    on roles (:app) do
      within "#{current_path}" do
        with rails_env: "#{fetch(:stage)}" do
          execute "cd /home/#{fetch(:user)}/apps/#{fetch(:application)}/current && rake resque:work QUEUE='*'"
        end
      end
    end
  end
end