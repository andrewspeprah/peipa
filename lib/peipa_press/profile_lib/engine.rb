module PeipaPress::ProfileLib::Engine
  
  def self.process(options)
  	# Search for press if
  	# service is not registration
		# Find country
		params = options[:params]
		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  return result unless result[:success]
	  options[:formatted_msisdn] = result[:data][:msisdn]
		
		# Find country
		options[:country] = Country.find_by_name(params[:country])
		return {success: false, message: "Selected country is unavailable"} unless options[:country].present?

  	# Switch between libraries with respect to service
  	result =
	  	case options[:service]
      when "login"
        PeipaPress::ProfileLib::Login.process(options)
      when "logout"
        PeipaPress::ProfileLib::Logout.process(options)
      when "verify"
        PeipaPress::ProfileLib::Verify.process(options)
      when "update_profile"
        PeipaPress::ProfileLib::UpdateProfile.process(options)
      when "details"
				PeipaPress::ProfileLib::Details.process(options)
			when "device_update"
        PeipaPress::ProfileLib::DeviceUpdate.process(options)
	  	end

	  return result
  end
end