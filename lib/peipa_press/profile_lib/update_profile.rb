module PeipaPress::ProfileLib::UpdateProfile
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Check whether user is already registered
		options[:press] = options[:country].presses.find_by_msisdn(options[:formatted_msisdn])
		return {success: false, message: "You are not registered to use this platform"} unless options[:press].present?
		
#		 requires :msisdn,             type: String, desc: 'Telephone number of customer.'
#    requires :country,            type: String, desc: 'Country of customer.'
#    requires :name,               type: String, desc: 'Name of the printing press'
#    requires :info,               type: String, desc: 'Information about the press'
#    requires :location_address,   type: String, desc: 'Location address of press'
#    requires :telephone,          type: String, desc: "Telephone of printing press"
#    requires :location_pic,       type: String, desc: "Location pic url"
#    requires :profile_pic,        type: String, desc: "Profile picture of press"
# {"id"=>nil, "name"=>"", "info"=>"", "location_address"=>"",
# "telephone"=>"", "is_active"=>true, "is_verified"=>false,
# "created_at"=>nil, "updated_at"=>nil, "msisdn"=>nil,
# "country_id"=>0, "uid"=>nil, "pic_url"=>"",
# "location_pic_url"=>"", "is_approved"=>false}
	
		press = options[:press]
		press.name = params[:name]
		press.info = params[:info]
		press.location_address = params[:location_address]
		press.telephone = params[:telephone]
		
		if params[:location_pic].present?
			press.location_pic_url = params[:location_pic]
		end

		if params[:profile_pic].present?
			press.pic_url = params[:profile_pic]
		end
		
		if press.save
			format_response(options,"Profile successfully saved",true)
		else
			format_response(options,"Error saving profile. Please try again")
		end
	end

	# Formats press response
	# and sends to user
	def self.format_response(options,message, success = false)
		press = options[:press]
		return {success: success, message: message,
							data: {
							press: {
									id: press.id || "",
									name: press.name || "",
									info: press.info || "",
									location_address: press.location_address || "",
									telephone: press.telephone || "",
									is_active: press.is_active,
									is_verified: press.is_verified,
									created_at: press.created_at,
									updated_at: press.updated_at,
									msisdn: press.msisdn,
									uid: press.uid || "",
									pic_url: press.pic_url,
									location_pic_url: press.location_pic_url,
									is_approved: press.is_approved,
									country: press.country.name
								},
							account: {
								balance: press.account.balance.to_f,
								currency: press.account.balance.currency.code
							}
							}
						}
	end
end