module PeipaPress::ProfileLib::Login
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Check whether user is already registered
		options[:press] = options[:country].presses.find_by_msisdn(options[:formatted_msisdn])

		# If the press is present
		# TODO: Check whether is the same device
		if options[:press].present?
			# If press is not verified
			# send verification log
			# After save send a verification code to press number
			options[:user] = options[:press]
			unless options[:user].is_verified
				result = Peipa::SmsLib::Verification.send_verification(options)
			end
			
			# Enable device for user
			options[:press].device.enable!

			return format_response(options)
		end


		# If the press is not present
		# create new press and allow user 
		# to verify the account
		return Peipa::PressLib::Registration.process(options)
	end

	# Formats press response
	# and sends to user
	def self.format_response(options)
		press = options[:press]
		return {success: true, message: "Press information details",
							data: {
							press: {
									id: press.id || "",
									name: press.name || "",
									info: press.info || "",
									location_address: press.location_address || "",
									telephone: press.telephone || "",
									is_active: press.is_active,
									is_verified: press.is_verified,
									created_at: press.created_at,
									updated_at: press.updated_at,
									msisdn: press.msisdn,
									uid: press.uid || "",
									pic_url: press.pic_url,
									location_pic_url: press.location_pic_url,
									is_approved: press.is_approved,
									country: press.country.name
								},
							account: {
								balance: press.account.balance.to_f,
								currency: press.account.balance.currency.code
							}
							}
						}
	end
end