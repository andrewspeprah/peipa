module PeipaPress::ProfileLib::Logout
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Check whether user is already registered
		options[:press] = options[:country].presses.find_by_msisdn(options[:formatted_msisdn])

		# If the press is present
		# TODO: Check whether is the same device
		if options[:press].present?
			options[:press].device.disable!
			return {success: true, message: "Disabled press device after logout"}
		end
	end
end