module PeipaPress::ProfileLib::DeviceUPdate
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Check whether user is already registered
		options[:press] = options[:country].presses.find_by_msisdn(options[:formatted_msisdn])
		return {success: false, message: "You are not registered to use this platform"} unless options[:press].present?
    
    # Get Press' device
    device = press.device
    return {success: false, message: "Device not present"} unless device.present?

		if device.update_attributes(fcm_id: params[:fcm_id], uid: params[:device_uid])
			format_response(options,"Profile successfully saved",true)
		else
			format_response(options,"Error saving profile. Please try again")
		end
	end

	# Formats press response
	# and sends to user
	def self.format_response(options,message, success = false)
		press = options[:press]
		return {success: success, message: message,
							data: {
							press: {
									id: press.id || "",
									name: press.name || "",
									info: press.info || "",
									location_address: press.location_address || "",
									telephone: press.telephone || "",
									is_active: press.is_active,
									is_verified: press.is_verified,
									created_at: press.created_at,
									updated_at: press.updated_at,
									msisdn: press.msisdn,
									uid: press.uid || "",
									pic_url: press.pic_url,
									location_pic_url: press.location_pic_url,
									is_approved: press.is_approved,
									country: press.country.name
								},
							account: {
								balance: press.account.balance.to_f,
								currency: press.account.balance.currency.code
							}
							}
						}
	end
end