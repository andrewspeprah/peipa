module PeipaPress::AccountLib::Withdrawal

  def self.process(options)
    params = options[:params]
    press = options[:press]

    # Set default success
    success = false
    email_options = {}
    email_message = "Please attend to this withdrawal request<br><b>Press Details</b>:<br>"\
                          "<center><table>"\
                          "<tr><td>Press Name</td><td>#{press.name}</td></tr>"\
                          "<tr><td>Location</td><td>#{press.location_address}</td></tr>"\
                          "<tr><td>Telephone</td><td>#{press.telephone}</td></tr></table></center>"

    # Check amount
    return {success: false, message: "Amount not present"} unless params[:amount].present?

    # Check the category of withdrawal
    # the make the necessary request and notification
    # <AccountHistory id: nil, amount_in_pesewas: nil, service: nil,
    # remarks: nil, transaction_id: nil, account_id: nil,
    # new_balance_in_pesewas: 0, created_at: nil,
    # updated_at: nil, currency: "GHS", transaction_type: "", params: {} >
    details = params[:details].split("|")
    case params[:category]
      when "bank_account"
        bank_name = details[0]
        bank_branch = details[1]
        account_name = details[2]
        account_no = details[3]
        recipient_telephone = details[4]

        # Create an account history and send email to
        # CFO group for action to be taken
        
        if press.account
                .account_histories
                .create(amount: params[:amount].to_money,
                       service: "withdrawal_request",
                       remarks: "Withdrawal to Bank Account.\n#{bank_name}\n#{bank_branch}\n#{account_name}\n#{account_no}",
                       params: {bank_name: bank_name,
                                bank_branch: bank_branch,
                                account_name: account_name,
                                account_no: account_no,
                                recipient_telephone: recipient_telephone})

          # Send email and SMS to CFO
          email_message += "<br><b>Withdrawal Details:</b>:"\
                           "<center><table>"\
                           "<tr><td>Withdrawal type</td><td>Cash</td></tr>"\
                           "<tr><td>Bank Name</td><td>#{bank_name}</td></tr>"\
                           "<tr><td>Bank Branch</td><td>#{bank_branch}</td></tr>"\
                           "<tr><td>Account Name</td><td>#{account_name}</td></tr>"\
                           "<tr><td>Account Number</td><td>#{account_no}</td></tr>"\
                           "<tr><td>Recipient Telephone</td><td>#{recipient_telephone}</td></tr>"\
                           "<tr><td>Amount (#{params[:amount].to_money.currency.iso_code})</td><td>#{params[:amount]}</td></tr>"\
                           "</table></center>"
          email_options = {subject: "WITHDRAWAL REQUEST - BANK ACCOUNT", message: email_message}
          success = true
        end
      when "mobile_money"
        recipient_telephone = details[0]
        recipient_name = details[1]

        if press.account
               .account_histories
               .create(amount: params[:amount].to_money,
                       service: "withdrawal_request",
                       remarks: "Withdrawal to Mobile Money Wallet.\n#{recipient_telephone}\n#{recipient_name}",
                       params: {recipient_telephone: recipient_telephone,
                                recipient_name: recipient_name})

          email_message += "<br><b>Withdrawal Details:</b>:"\
                           "<center><table>"\
                           "<tr><td>Withdrawal type</td><td>Cash</td></tr>"\
                           "<tr><td>Recipient Name</td><td>#{recipient_name}</td></tr>"\
                           "<tr><td>Recipient Telephone</td><td>#{recipient_telephone}</td></tr>"\
                           "<tr><td>Amount (#{params[:amount].to_money.currency.iso_code})</td><td>#{params[:amount]}</td></tr>"\
                           "</table></center>"
          email_options = {subject: "WITHDRAWAL REQUEST - MOBILE MONEY", message: email_message}
          success = true
        end
      when "cash"
        recipient_name = details[0]
        recipient_telephone = details[1]

        if press.account
               .account_histories
               .create(amount: params[:amount].to_money,
                       service: "withdrawal_request",
                       remarks: "Request for Cash.\n#{recipient_name}\n#{recipient_telephone}",
                       params: {recipient_name: recipient_name,
                                recipient_msisdn: recipient_telephone})

          email_message += "<br><b>Withdrawal Details:</b>:"\
                           "<center><table>"\
                           "<tr><td>Withdrawal type</td><td>Cash</td></tr>"\
                           "<tr><td>Recipient Name</td><td>#{recipient_name}</td></tr>"\
                           "<tr><td>Recipient Telephone</td><td>#{recipient_telephone}</td></tr>"\
                           "<tr><td>Amount (#{params[:amount].to_money.currency.iso_code})</td><td>#{params[:amount]}</td></tr>"\
                           "</table></center>"

          email_options = {subject: "WITHDRAWAL REQUEST - CASH", message: email_message}
          success = true
        end
    end

    if success
      # Resque.enqueue(WithdrawalEmailWorker, email_options)
      WithdrawalEmailWorker.perform_async(email_options)
      message = "We've received your withdrawal request. Payment would be made as quick as possible. Thanks for using Peipa."
    else
      message = "We encountered an error while saving your withdrawal request. Please try again."
    end

    # Send response to mobile app
    return {success: success, message: message}
  end
end