module PeipaPress::OrderLib::Status
  
  def self.process(options)
		# Switch between libraries with respect to service
		# params
		params = options[:params]

		# Structure reponse
		# Add time helper
		order = options[:press].orders.find_by_uid(params[:order_uid])
		return { success: false, message: "Order not found" } unless order.present?

		# Use the state of the order to
		# perform action
		case params[:state]
		when "reviewed"
			review_order(order: order)
		when "rejected"
			reject_order(order: order, comment: params[:comment])
		when "complete"
			complete_order(order: order, comment: params[:comment])
		when "delete"
			# If order is not rejected
			# for reject order then archive
			unless order.rejected?
				reject_order(order: order, comment: "Order deleted")
			end

			# check if order is already archived
			if order.archived?("press")
				return send_response({order: order, message: "Order is already removed."}, false)
			end

			order.archive!("press")
			send_response(order: order, message: "Order removed.")
		end
  end

  # Sends confirmation notice to user
  # and changes from pending to reviewed
  def self.review_order(options)
  	order = options[:order]
  	order.reviewed!

		# Send confirmation to customer
		# Send notification to press
		options = {country_id: order.customer.country.id,
					user_id: order.customer.id, 
					user_type: "customer", 
					message: "Please confirm your order-#{order.uid}", 
					collapse_key: "confirm_order"}
		# Resque.enqueue(FcmWorkers::NotifyWorker,options)
		NotifyWorker.perform_async(options)

    # Send response to press
  	send_response(order: order, message: "Confirmation notice has been sent to customer")
  end

  # Send rejection to customer
  # When order is rejected
  # customer can only retry sending the order
  # When order is reject a reason has to be added
  def self.reject_order(options)
  	order = options[:order]

		# If order has already been
		# confirmed, reverse the 
		# amount taken from customer
		if order.confirmed?
			reverse_confirmation(order)
		end

		# Now reject the order
  	order.rejected!(options[:comment],order.press.name)

		# Send confirmation to customer
		# Send notification to press
	options = {country_id: order.customer.country.id,
			user_id: order.customer.id, 
			user_type: "customer", 
			message: "Sorry, your order has been rejected.-#{order.uid}", 
			collapse_key: "rejected_order"}
	# Resque.enqueue(FcmWorkers::NotifyWorker,options)
	NotifyWorker.perform_async(options)

    # Send response to press
  	send_response(order: order, message: "Rejection notice has been sent to customer.")
  end

  # Send rejection to customer
  # When order is rejected
  # customer can only retry sending the order
  # When order is reject a reason has to be added
  def self.complete_order(options)
  	order = options[:order]
  	order.order_complete!

		# Send confirmation to customer
		# Send notification to press
	options = {country_id: order.customer.country.id,
				user_id: order.customer.id, 
				user_type: "customer", 
				message: "Your order is complete.-#{order.uid}", 
				collapse_key: "complete_order"}
	# Resque.enqueue(FcmWorkers::NotifyWorker,options)
	NotifyWorker.perform_async(options)

    # Send response to press
  	send_response(order: order, message: "Notification is sent to customer.")
  end

  # Formats order response and sends to
  # device
  def self.send_response(options, success = true)
  	order = options[:order]

		press = order.press
		order_specs = order.specs
		delivery = order.delivery

		
		return { success: success, 
						 message: options[:message], 
						 data: { 
						 	grand_total: "#{order.amount.currency}#{(order.amount + order.additional_cost).to_f}",
						 	press: {name: press.try(:name),
						 					location_address: "#{press.try(:location_address)}\n#{press.try(:telephone)}",
						 					location_url: press.location_pic_url},
						 	document_details: {files: order.documents.pluck(:filename).join("\n"),
						 					documents: order.documents.select(:file_url,:filename),
						 					service: order_specs["service_type"],
						 					doc_type: order_specs["document"]["name"], 
						 					paper_type: "#{order_specs['paper_type']['size']},#{order_specs['paper_type']['ptype']}",
						 					color: "#{order_specs['paper_type']['color']}",
						 					sub_total: "#{order.amount.currency}#{order.amount.to_f - order.copies_amount.to_f}",
						 					no_of_sheets: "#{order.no_of_sheets}"
						 				},
						 	copies_details: {
						 					has_copies: order_specs["make_copies"] ? "Yes" : "No",
						 					no_of_copies: order_specs["no_of_copies"].to_i.to_s,
						 					paper_type: "#{order_specs['copy_paper_type'].try(:[],'size')},#{order_specs['copy_paper_type'].try(:[],'ptype')}",
						 					color: "#{order_specs['copy_paper_type'].try(:[],'color')}",
						 					sub_total: "#{order.amount.currency}#{order.copies_amount.to_f}"
						 			},
						 	additional_info: order_specs["additional_info"] || "",
						 	additional_cost: "#{order.amount.currency}#{order.additional_cost.to_f}",
						 	delivery: {
						 			runner: "#{delivery.runner.try(:name)} \n #{delivery.runner.try(:telephone)}",
						 			pic_url: "#{delivery.runner.try(:pic_url)}",
						 			status: "#{delivery.status}"
						 		},
						 	customer: {
						 			name: order.customer.full_name,
						 			address: "#{order.customer.uid} \n #{order.customer.email} \n #{order.customer.msisdn}"
						 		},
						 	state: order.state
							}
					}
  end

  # Reverses confirmation
  # and gives customer's money back
  def self.reverse_confirmation(order)
  	# Check customer's balance before confirming transaction
  	order_price = (order.amount + order.additional_cost)
  	order.customer.account.credit!(amount: order_price, service: "payment_reversal", remarks: "Order confirmation payment reversed - #{order.uid}")
  end

end