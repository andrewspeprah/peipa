module PeipaPress::OrderLib::Detail
  
  def self.process(options)
		# Switch between libraries with respect to service
		# params
		params = options[:params]

		# Structure reponse
		# Add time helper
		order = options[:press].orders.find_by_uid(params[:order_uid])
		if order.present?
			press = order.press
			order_specs = order.specs
			delivery = order.delivery

			return { success: true, 
							 message: "Order found", 
							 data: { 
							 	grand_total: "#{order.amount.currency}#{(order.amount+ order.additional_cost).to_f}",
							 	press: {name: press.try(:name),
							 					location_address: "#{press.try(:location_address)}\n#{press.try(:telephone)}",
							 					location_url: press.location_pic_url},
							 	document_details: {files: order.documents.pluck(:filename).join("\n"),
							 					documents: order.documents.select(:file_url,:filename),
							 					service: order_specs["service_type"],
							 					doc_type: order_specs["document"]["name"], 
							 					paper_type: "#{order_specs['paper_type']['size']},#{order_specs['paper_type']['ptype']}",
							 					color: "#{order_specs['paper_type']['color']}",
							 					sub_total: "#{order.amount.currency}#{order.amount.to_f - order.copies_amount.to_f}",
							 					no_of_sheets: "#{order.no_of_sheets}"
							 				},
							 	copies_details: {
							 					has_copies: order_specs["make_copies"] ? "Yes" : "No",
							 					no_of_copies: order_specs["no_of_copies"].to_i.to_s,
							 					paper_type: "#{order_specs['copy_paper_type'].try(:[],'size')},#{order_specs['copy_paper_type'].try(:[],'ptype')}",
							 					color: "#{order_specs['copy_paper_type'].try(:[],'color')}",
							 					sub_total: "#{order.amount.currency}#{order.copies_amount.to_f}"
							 			},
							 	additional_info: order_specs["additional_info"] || "",
							 	additional_cost: "#{order.amount.currency}#{order.additional_cost.to_f}",
							 	delivery: {
							 			runner: "#{delivery.runner.try(:name)} \n #{delivery.runner.try(:telephone)}",
							 			pic_url: "#{delivery.runner.try(:pic_url)}",
							 			status: "#{delivery.status}"
							 		},
							 	customer: {
							 			name: order.customer.full_name,
							 			address: "#{order.customer.uid} \n #{order.customer.email} \n #{order.customer.msisdn}"
							 		},
							 	state: order.state
								}
						}
		else
			return { success: false, message: "Order not found" }
		end
  end

end