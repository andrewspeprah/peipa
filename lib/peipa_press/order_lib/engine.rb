module PeipaPress::OrderLib::Engine
  
  def self.process(options)
  	#---------------------- VALIDATION ----------------------
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  return result unless result[:success]

	  # Get formatted msisdn
	  options[:formatted_msisdn] = result[:data][:msisdn]

  	# Customer
		options[:press] = Press.find_by_msisdn(options[:formatted_msisdn])
		# Cancel if customer is not found
		return {success: false, message: "You are not registered on the platform. Please enter telephone number again."} unless options[:press].present?

		# Check country and set currency
		result = Peipa::Util.get_currency(params)
		return result unless result[:success]
		options[:currency] = result[:data][:currency_code]

  	# Switch between libraries with respect to service
  	result =
	  	case options[:service]
	  	when "list"
	  		PeipaPress::OrderLib::List.process(options)
	  	when "details"
	  		PeipaPress::OrderLib::Detail.process(options)
	  	when "status"
	  		PeipaPress::OrderLib::Status.process(options)
	  	when "comments"
	  		PeipaPress::OrderLib::Comments.process(options)
	  	end

	  return result
  end
end