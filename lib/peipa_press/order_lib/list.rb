module PeipaPress::OrderLib::List
  
  def self.process(options)
		# Switch between libraries with respect to service
		# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Structure reponse
		# Add time helper
		include ActionView::Helpers::DateHelper
		orders = []
		page = params[:page] || 1
		limit = params[:per_page] || 15
		paginate = params[:paginate] == "true" ? true : false
		if paginate
			return {success: true, message: "List of Orders",
							data: {orders: options[:press].orders
										 .where("((uid LIKE ?) OR (customer_name LIKE ?)) AND is_archived = false AND archive_flags LIKE ?",
										 				"\%#{params[:query]}\%",
										 				"\%#{params[:query]}\%",
										 				"_,0,_")
										 .order("created_at DESC")
										 .page(page)
										 .per(limit)
										 .map{|order| generate_order(order) }
										}
						}
	 	else
			return {success: true, message: "List of orders",
							data: {orders: options[:press].orders
											 .where("((uid LIKE ?) OR (customer_name LIKE ?)) AND is_archived = false AND archive_flags LIKE ?",
											 				"\%#{params[:query]}\%",
											 				"\%#{params[:query]}\%",
											 				"_,0,_")
											 .order("created_at DESC")
											 .map{ |order| generate_order(order) }
										}
						}
		end

  end

  # Generates order hash
  def self.generate_order(order)
  	return { currency: order.amount.currency.code,
			amount: order.amount.to_f,
			document: order.documents.pluck(:filename).join("\n"),
			press: order.press.name,
			moment_created: order.created_at.strftime("%d-%b-%Y %H:%M:%S"),
			order_uid: order.uid,
			state: order.state,
			service_type: order.specs["service_type"],
			customer: {
			 	name: order.customer.full_name,
			 	uid: order.customer.uid
			 	}
			}
  end

  # Get the relative time for the day
  def self.relative_time(start_time)
	  diff_seconds = Time.now - start_time
	  case diff_seconds
	    when 0 .. 59
	      puts "#{diff_seconds} seconds ago"
	    when 60 .. (3600-1)
	      puts "#{diff_seconds/60} minutes ago"
	    when 3600 .. (3600*24-1)
	      puts "#{diff_seconds/3600} hours ago"
	    when (3600*24) .. (3600*24*30) 
	      puts "#{diff_seconds/(3600*24)} days ago"
	    else
	      puts start_time.strftime("%m/%d/%Y")
	  end
	end
end