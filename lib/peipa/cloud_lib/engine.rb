module Peipa::CloudLib::Engine
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	result =
	  	case options[:service]
	  	when "list"
	  		Peipa::CloudLib::List.process(options)
	  	else
	  		{success: false, message: "Request was not complete. Please try again"}
	  	end

	  return result
  end
end