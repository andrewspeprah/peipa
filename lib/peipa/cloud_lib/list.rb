module Peipa::CloudLib::List
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

	  # find country
	  country = Country.find_by_name(params[:country])
	  return {success: false, message: "Country not found"} unless country.present?

		# Customer
		customer = country.customers.find_by_msisdn(formatted_msisdn)

		# Cancel if customer is not found
		unless customer.present?
			return {success: false, message: "You are not registered on the platform. Please enter telephone number again."}
		end

		# Return list of documents to user
		return {success: true,
						message: "List of files",
						data: {
							documents: customer.documents
																 .order("filename").map{|doc| {id: doc.id,
																											 file_url: doc.file_url,
																											 filename: doc.filename,
																											 created_at: doc.created_at.strftime("%d-%b-%Y")}}
						}}
  end
end