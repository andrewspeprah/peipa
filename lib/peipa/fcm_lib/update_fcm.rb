module Peipa::FcmLib::UpdateFcm
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		user = 
			case params[:user_type]
			when "customer"
				options[:country].customers.find_by_msisdn(options[:formatted_msisdn])
			when "press"
				options[:country].presses.find_by_msisdn(options[:formatted_msisdn])
			when "runner"
				options[:country].runners.find_by_msisdn(options[:formatted_msisdn])
			end

		return {success: false, message: "#{params[:user_type]} not present"} unless user.present?

		# Update FCM
		if user.device.update_attributes(fcm_id: params[:fcm_id])
			return {success: true, message: "FCM updated"}
		else
			return {success: false, message: "Error updating FCM ID"}
		end
  end
end