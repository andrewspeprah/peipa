module Peipa::FcmLib::Engine
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  return result unless result[:success]
	  options[:formatted_msisdn] = result[:data][:msisdn]

	  # Country
	  options[:country] = Country.find_by_name(params[:country])
	  return {success: false, message: "Country not present"} unless options[:country].present?
	  
  	# Switch between libraries with respect to service
  	result =
	  	case options[:service]
	  	when "update_fcm"
	  		Peipa::FcmLib::UpdateFcm.process(options)
	  	end

	  return result
  end
end