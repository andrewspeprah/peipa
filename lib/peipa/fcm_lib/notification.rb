module Peipa::FcmLib::Notification
  
  def self.process(options)
    # Log notification
    
  	device = options[:user].try(:device)
    
  	return {success: false, message: "Device not available"} unless device.present?
    return {success: false, message: "Device not active"} unless device.is_active

		fcm = FCM.new(ENV["FCM_SERVER_KEY"])
		# you can set option parameters in here
		#  - all options are pass to HTTParty method arguments
		#  - ref: https://github.com/jnunemaker/httparty/blob/master/lib/httparty.rb#L29-L60
		#  fcm = FCM.new("my_server_key", timeout: 3)
		registration_ids = [device.fcm_id] # an array of one or more client registration tokens
		message = options[:message]
    notification_type = options[:collapse_key] || "notification"
		options = {
          priority: 'high',
          data: {
            message: "#{notification_type}~#{message}"
          },
          notification: {
            body: message,
            sound: 'default'
          }
        }
		response = fcm.send(registration_ids, options)
  end
end