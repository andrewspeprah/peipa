module Peipa::FilesLib::FileMetadata

  def self.process(options)
    # Get document Id and find
    # documnet
    document = Document.find(options[:document_id])

    # if document is not found
    # send a document not found message
    unless document.present?
      return {success: false, message: "Document not found"}
    end

		# Download all documents and get their metadata
		# after that delete file from the server
    page_count = 0

    result = download_file(document)
    if result.etag.include? document.md5_hash
      # If all files are downloaded
      # then we can read the files and
      # calculate the pages
      result = generate_metadata(document)
      result = result.symbolize_keys
      page_count += (result[:metadata]["xmpTPg:NPages"].to_i == 0 ? 1 : result[:metadata]["xmpTPg:NPages"].to_i)
    end

		# Save page count
		if document.update_attributes(page_count: page_count, is_calculated: true)
      return { success: true, message: "Number of pages successfully updated." }
    else
      return { success: false, message: "Error while saving number of pages in document." }
    end
  end


  # Downloads document from s3 bucket
  def self.download_file(document)
  	# check if file exists
  	unless File.exists? document_path("#{document.md5_hash}#{document.filename}")
	  	# if result is unsuccessful retry again
	  	#  https://s3-eu-west-1.amazonaws.com/peipadocs/ghana/PC233500011722/mnt/sdcard/Documents/Document.docxw
	  	return $amazon_s3.bucket(ENV['BUCKET_NAME']).object(document.file_url).get(response_target: document_path("#{document.md5_hash}#{document.filename}"))
    else
      return document
	  end
  end

  # Get document path
  def self.document_path(filename)
  	return Rails.root.join('app','docs',filename)
  end

  # Gets the metadata of file
  def self.generate_metadata(document)
  	if document.metadatum.try(:params).present?
  		return document.metadatum.params
  	else
	  	yomu = Henkei.new document_path("#{document.md5_hash}#{document.filename}")
	  	document.metadatum.update_attribute(:params, {mimetype: yomu.mimetype.extensions, metadata: yomu.metadata})

      # Delete file after reading metadata
      if File.exists? document_path("#{document.md5_hash}#{document.filename}")
        File.delete document_path("#{document.md5_hash}#{document.filename}")
      end

	  	return {mimetype: yomu.mimetype.extensions,metadata: yomu.metadata}
	  end
  end

end