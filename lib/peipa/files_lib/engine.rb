module Peipa::FilesLib::Engine
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  return result unless result[:success]
	  options[:formatted_msisdn] = result[:data][:msisdn]

	  # Country
	  options[:country] = Country.find_by_name(params[:country])
	  return {success: false, message: "Country not present"} unless options[:country].present?
	  

		# Customer
		options[:customer] = options[:country].customers.find_by_msisdn(options[:formatted_msisdn])
		return {success: false, message: "You are not registered on the platform. Please enter telephone number again."} unless options[:customer].present?

  	# Switch between libraries with respect to service
  	result =
	  	case options[:service]
	  	when "verify"
	  		Peipa::FilesLib::FileVerify.process(options)
	  	when "save"
				Peipa::FilesLib::FileSave.process(options)
			when "metadata"
				Peipa::FilesLib::FileMetadata.process(options)
			when "pages"
				Peipa::FilesLib::FilePages.process(options)
	  	end

	  return result
  end
end