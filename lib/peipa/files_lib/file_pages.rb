module Peipa::FilesLib::FilePages
  
  def self.process(options)
    # Switch between libraries with respect to service
    params = options[:params]

    # set parameters
    page_count = 0
    all_calculated = params[:documents].present?

    # Split document into md5 hashes
    documents = params[:documents].split("--|--")
    documents.each do |document_md5|
      document = options[:customer].documents.find_by_md5_hash(document_md5.split("---~---")[1])
      if document.present?
        all_calculated &= document.is_calculated
        page_count += document.page_count
      end
    end

    if all_calculated
      if page_count <= 0
        return {success: false, message: "Document pages calculation incomplete.",
                data: {number: page_count,
                       charge_specs: options[:country].charge_specs.order(:amount_in_pesewas).as_json}}
      else
        return {success: true, message: "Document pages calculation complete.",
                data: {number: page_count,
                       charge_specs: options[:country].charge_specs.order(:amount_in_pesewas).as_json}}
      end
    else
      return {success: false,
              message: "Document pages calculation not complete.",
              data: {number: 0,
                charge_specs: options[:country].charge_specs.order(:amount_in_pesewas).as_json}}
    end
  end
end