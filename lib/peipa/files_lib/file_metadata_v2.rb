module Peipa::FilesLib::FileMetadataV2

  def self.process(options)
    params = options[:params]
    # Get document Id and find
    # documnet
    document = Document.find_by_md5_hash(params[:md5_hash])

    # if document is not found
    # send a document not found message
    unless document.present?
      # Create new document and associate to customer
      filepath = params[:FilePath].gsub("s3://peipadbapp/","")
      # <Document id: nil, file_url: nil, filename: nil,
      # is_archived: false, customer_id: nil,
      # created_at: nil, updated_at: nil,
      # md5_hash: nil, page_count: 0, is_calculated: false> 
      filepath = filepath.split("/")

      # Find the customer
      customer_id = Customer.find_by_msisdn(filepath[1].gsub("PC","")).try(:id)

      # Save the document
      document = Document.create(file_url: filepath.join("/"),
                                 filename: filepath.last,
                                 is_archived: false,
                                 customer_id: customer_id,
                                 md5_hash: params[:md5_hash])
    end

		# Download all documents and get their metadata
		# after that delete file from the server
    page_count = params[:page_count].to_i
    document.metadatum.update_attribute(:params, {mimetype: params[:mimetype], metadata: params[:metadata]})

		# Save page count
    if document.update_attributes(page_count: page_count, is_calculated: true)
      order_ids = document.customer.orders.where(state: "calculating_estimate").select([:id]).pluck(:id)
      order_documents = OrderDocument.where("order_id IN (?) AND document_id = ?",order_ids, document.id)
      
      if order_documents.present?
        order_documents.each do |order|
          options[:order_id] = order.id
          # Calculate the estimated price
          # Resque.enqueue(CalculatorWorker,options.as_json)
          CalculatorWorker.perform_async(options.as_json)          
        end
      end

      return { success: true, message: "Number of pages successfully updated." }
    else
      return { success: false, message: "Error while saving number of pages in document." }
    end
  end

end