module Peipa::FilesLib::FileVerify
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Get presses of which customer
		# belongs to
		filename = get_file_name(params[:filename])
		document = options[:customer].documents.find_by_md5_hash(params[:md5_hash])
		return {success: false, message: "File not found"} unless document.present?

		# if file is present but not calculated
		# calculate
		if document.is_calculated
			return {success: true, message: "Files are the same.", data: {document: document.as_json}}
		else
			# Pass params to worker
			options[:document_id] = document.id
			# Resque.enqueue(MetadataWorker, options)
			MetadataWorker.perform_async(options)
			
			
			return {success: true, message: "File found but not calculated.", data: {document: document.as_json}}
		end
  end

    # Gets the file name
  def self.get_file_name(document)
  	document.split("/").last
  end
end