module Peipa::GpLib::GpRegister
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Get customer's devices
		# being used
		if options[:customer].gp.update_location(lat: params[:lat], lng: params[:lng])
			return {success: true, message: "location updated"}
		else
			return {success: false, message: "Error updating location"}
		end
  end
end