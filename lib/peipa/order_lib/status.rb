module Peipa::OrderLib::Status
  
  def self.process(options)
		# Switch between libraries with respect to service
		# params
		params = options[:params]

		# Structure reponse
		# Add time helper
		order = options[:customer].orders.find_by_uid(params[:order_uid])
		return { success: false, message: "Order not found" } unless order.present?

		# Use the state of the order to
		# perform action
		case params[:state]
		when "confirmed"
			return confirm_order(order: order)
		when "rejected"
			return reject_order(order: order, comment: params[:comment])
		when "delete"
			unless order.rejected?
				reject_order(order: order, comment: "Order deleted")
			end
			order.archive!("customer")
			return send_response(success: true, order: order, message: "Order removed.")
		when "collectOrder"
			return collect_order(order: order, comment: params[:comment])
		when "requestDelivery"
			return deliver_order(order: order, comment: params[:comment])
		end
  end

  # Sends confirmation notice to user
  # and changes from pending to reviewed
  def self.confirm_order(options)
		# Else confirm order
  	order = options[:order]

  	return {success: false, message: "Order has already been confirmed."} if order.confirmed?

  	# Check customer's balance before confirming transaction
  	customer_balance = order.customer.account.balance
  	order_price = order.amount + order.additional_cost

  	if customer_balance >= order_price
  		new_balance = order.customer.account.debit!(amount: order_price, service: "order_confirmation", remarks: "Order confirmation - #{order.uid}")

  		# if debit fails
  		# send a notification back to customer
  		unless new_balance
  			return send_response(success: false, order: order, message: "Please try confirming order again.")
  		end

	  	order.confirmed!

			# Send confirmation to customer
			# Send notification to press
		options = {country_id: order.press.country.id,
					user_id: order.press.id, 
					user_type: "press", 
					message: "Order confirmed by customer-#{order.uid}", 
					collapse_key: "new_order"}
		# Resque.enqueue(FcmWorkers::NotifyWorker,options)
	    NotifyWorker.perform_async(options)
		

	    # Send response to press
	  	return send_response(success: true, order: order, message: "Your order has been confirmed. Your remainig balance is #{order.amount.currency.code}#{new_balance / 100.0}")
	  else
		  # Send notification to customer
		options = {country_id: order.press.country.id,
			user_id: order.press.id, 
			user_type: "press", 
			message: "Customer has insufficient funds to confirm the order-#{order.uid}", 
			collapse_key: "new_order"}
		# Resque.enqueue(FcmWorkers::NotifyWorker,options)
	    NotifyWorker.perform_async(options)
		

		  # Send notification to customer
		  options = {country_id: order.customer.country.id,
			user_id: order.customer.id, 
			user_type: "customer", 
			message: "You have insufficient funds to confirm the order-#{order.uid}", 
			collapse_key: "new_order"}
		# Resque.enqueue(FcmWorkers::NotifyWorker,options)
	    NotifyWorker.perform_async(options)
		

	    return send_response(success: false, order: order, message: "You have insufficient balance to confirm order. Current Balance: #{order.amount.currency.code}#{customer_balance}")
	  end
  end

  # Send rejection to customer
  # When order is rejected
  # customer can only retry sending the order
  # When order is reject a reason has to be added
  def self.reject_order(options)
  	order = options[:order]
  	order.rejected!(options[:comment],order.customer.full_name)

  	# If order has already been
		# confirmed, reverse the 
		# amount taken from customer
		if order.confirmed?
			reverse_confirmation(order)
		end


		# Send confirmation to customer
		# Send notification to press
		options = {country_id: order.customer.country.id,
			user_id: order.press.id, 
			user_type: "press", 
			message: "Sorry, your order has been cancelled by customer.-#{order.uid}", 
			collapse_key: "new_order"}
	# Resque.enqueue(FcmWorkers::NotifyWorker,options)
    NotifyWorker.perform_async(options)
	

    # Send response to press
  	return send_response(success: true, order: order, message: "Rejection notice has been sent to press.")
  end

  # Formats order response and sends to
  # device
  def self.send_response(options)
  	order = options[:order]

		press = order.press
		order_specs = order.specs
		delivery = order.delivery

		
		return { success: options[:success] || false, 
						 message: options[:message], 
						 data: { 
						 	grand_total: "#{order.amount.currency}#{(order.amount + order.additional_cost).to_f}",
						 	press: {name: press.try(:name),
						 					location_address: "#{press.try(:location_address)}\n#{press.try(:telephone)}",
						 					location_url: press.location_pic_url},
						 	document_details: {files: order.documents.pluck(:filename).join("\n"),
						 					documents: order.documents.select(:file_url,:filename),
						 					service: order_specs["service_type"],
						 					doc_type: order_specs["document"]["name"], 
						 					paper_type: "#{order_specs['paper_type']['size']},#{order_specs['paper_type']['ptype']}",
						 					color: "#{order_specs['paper_type']['color']}",
						 					sub_total: "#{order.amount.currency}#{order.amount.to_f - order.copies_amount.to_f}",
						 					no_of_sheets: "#{order.no_of_sheets}"
						 				},
						 	copies_details: {
						 					has_copies: order_specs["make_copies"] ? "Yes" : "No",
						 					no_of_copies: order_specs["no_of_copies"].to_i.to_s,
						 					paper_type: "#{order_specs['copy_paper_type'].try(:[],'size')},#{order_specs['copy_paper_type'].try(:[],'ptype')}",
						 					color: "#{order_specs['copy_paper_type'].try(:[],'color')}",
						 					sub_total: "#{order.amount.currency}#{order.copies_amount.to_f}"
						 			},
						 	additional_info: order_specs["additional_info"] || "",
						 	additional_cost: order.string_extra_charges,
						 	delivery: {
						 			runner: "#{delivery.runner.try(:name)} \n #{delivery.runner.try(:telephone)}",
						 			pic_url: "#{delivery.runner.try(:pic_url)}",
						 			status: "#{delivery.status}"
						 		},
						 	customer: {
						 			name: order.customer.full_name,
						 			address: "#{order.customer.uid} \n #{order.customer.email} \n #{order.customer.msisdn}"
						 		},
						 	state: order.state
							}
					}
  end

  # Collect order
  # sends notification to printing press about
  # order collection
  def self.collect_order(options)
  	order = options[:order]

  	# Create a collection log to verify the moemnt 
  	# a customerm took an orderw
  	if order.collection_logs.where(category: "press_customer").present?
  		return send_response(success: true, order: order, message: "Request already processed.")
  	end
  	
  	# Create a collection log
  	order.collection_logs.create(category: "press_customer", request_type: "customer_to_press")

  	# After delivery update the account
  	# balance of the printing press
  	order.press.account.credit!(amount: order.amount_without_fees, service: "order_completion", remarks: "Completing of order number - #{order.uid}")

  	# mark the order as delivered
  	# to the customer
  	order.delivered!

		# Send notification to press
		options = {country_id: order.press.country.id,
			user_id: order.press.id, 
			user_type: "press", 
			message: "Order collection.-#{order.uid}", 
			collapse_key: "order_collection"}
	# Resque.enqueue(FcmWorkers::NotifyWorker,options)
    NotifyWorker.perform_async(options)
	

    # Send response to press
  	return send_response(success: true, order: order, message: "Collection notice sent to press.")
  end


  # Deliver document
  def self.deliver_order(options)
  	order = options[:order]

		# Send notification to press
	options = {country_id: order.press.country.id,
		user_id: order.press.id, 
		user_type: "press", 
		message: "Delivery notice to runner.-#{order.uid}", 
		collapse_key: "delivery_notice"}
	# Resque.enqueue(FcmWorkers::NotifyWorker,options)
    NotifyWorker.perform_async(options)
	

    # Send response to press
  	return send_response(success: true, order: order, message: "Order is pending collection.")
  end

end