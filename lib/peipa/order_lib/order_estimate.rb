module Peipa::OrderLib::OrderEstimate
  
  def self.process(options)
		# Switch between libraries with respect to service
		# params
		params = options[:params]

		# Download all documents and get their metadata
		# after that delete file from the server
		# 
		complete_files = true
    page_count = 0
    all_calculated = true
    options[:order].documents.select([:page_count,:is_calculated]).each do |document|
      all_calculated &= document.is_calculated
			page_count += document.page_count
    end
  
    # wait for some minutes
    # and repost worker to check order estimate
    unless all_calculated
      return {success: false, message: "Order files have not been fully calculated."}
    end

		# Save page count
		options[:page_count] = page_count

		# When complete multiply
		# pages with order options
		result = calculate_order_amount(options)

    # Add number of pages to order
    result[:no_of_sheets] = page_count

    # Save the new amount
    options[:order].update_amount(result)

    # Notify press of order
    # Send notification to press
    press = options[:order].press
    options = {country_id: press.country.id,
      user_id: press.id, 
      user_type: "press", 
      message: "New Order-#{options[:order].uid}", 
      collapse_key: "new_order"}

    # Resque.enqueue(FcmWorkers::NotifyWorker,options)
    NotifyWorker.perform_async(options)
    

    return {success: true, message: "Order successfully calculated"}
  end

  # Calculate the amount needed for the order
  def self.calculate_order_amount(options)
  	order_specs = options[:order].specs

  	# Get the amount for original copies
  	original_price = pages_price(options[:page_count],order_specs,true)
  	dup_copy_price = pages_price(options[:page_count],order_specs)
    grand_total    = original_price + dup_copy_price

    # Calculate the charges
    charges        = calculate_charge(grand_total, options[:order])

    # Add charges to the total price
    # grand_total    += charges

  	return {grand_total: grand_total, copies_amount: dup_copy_price, charges: charges}
  end

  # Gets the original copy / duplicate copy price
  def self.pages_price(page_count, order_specs, original = false)
  	# sample order specs
  	# {
  	# "document"=>{"id"=>1, "name"=>"Document", "is_active"=>true, 
  	# "description"=>"Assignments, letters and other documents",
  	# "params"=>nil, "press_id"=>1, "created_at"=>"2017-09-02T08:42:44.998Z",
  	# "updated_at"=>"2017-09-02T08:42:44.998Z"},
  	# "paper_type"=>{"id"=>1, "size"=>"A4", "ptype"=>"Normal Paper", "color"=>"Black & White",
  	# "currency"=>"GHS", "unit"=>"PAGE", "service_id"=>1, "created_at"=>"2017-09-03T07:25:47.416Z",
  	# "updated_at"=>"2017-09-03T07:25:47.416Z", "price_in_pesewas"=>20},
  	# "copy_paper_type"=>{"id"=>1, "size"=>"A4", "ptype"=>"Normal Paper", "color"=>"Black & White",
  	# "currency"=>"GHS", "unit"=>"PAGE", "service_id"=>1, "created_at"=>"2017-09-03T07:25:47.416Z",
  	# "updated_at"=>"2017-09-03T07:25:47.416Z", "price_in_pesewas"=>20},
  	# "make_copies"=>false, "no_of_copies"=>"5", "service_type"=>"print"}
  	if original
  		price_per_page = Money.new(order_specs["paper_type"]["price_in_pesewas"],order_specs["paper_type"]["currency"])
  	else
      price_per_page = 0.to_money(order_specs["paper_type"]["currency"])

      if order_specs["make_copies"]
        price_per_page = Money.new(order_specs["copy_paper_type"]["price_in_pesewas"],order_specs["copy_paper_type"]["currency"])
  			price_per_page *= order_specs["no_of_copies"].to_i
      else
        price_per_page *= 0
  		end
  	end

  	price_per_page * page_count
  end

  # Calculates the fee
  # for printing
  def self.calculate_charge(grand_total,order)
    charge_specs = order.customer.country.charge_specs.where("amount_in_pesewas <= ? ",grand_total.cents).order("amount_in_pesewas DESC")

    # If charge spec is not present
    # return zero
    unless charge_specs.present?
      return 0.to_money
    end

    # If charge spec is present
    # return the charge for the work
    charge_spec = charge_specs.first

    # Add charges to order charges
    # | id | name | description | amount_in_pesewas | currency | is_accepted | created_at | updated_at |
    extra_charge =  ExtraCharge.new(name: "print_fees", 
                                    description: "Fee for printing order - #{order.uid}",
                                    amount: charge_spec.fee,
                                    currency: charge_spec.fee.currency.code,
                                    is_accepted: true)

    # If extra charge is saved,
    # send the charged amount
    if extra_charge.save
      order.extra_charges << extra_charge
      return charge_spec.fee
    end
      
    # Return zero as default
    return 0.to_money
  end
end