module Peipa::OrderLib::CreateOrder
  
  def self.process(options)
  	params = options[:params]

  	# Get press customer wants to put order
		press = options[:customer].country.try(:presses).try(:find_by_id, params[:press_id])
		return {success: false, message: "Printing Press not present on the system"} unless press.present?
		
		# if customer is verified send customer's details
		if press.present?
			# Search for service and paper type
			service = press.services.find_by_id(params[:document_type])
			return {success: false, message: "Selected service is not supported"} unless service.present?

			# Search for paper type
			paper_type = service.papers.find_by_id(params[:paper_type])
			return {success: false, message: "Selected paper type is not available"} unless paper_type.present?

			copy_paper_type = service.papers.find_by_id(params[:copy_paper_type])

			params.merge!(service: service, paper_type: paper_type, copy_paper_type: copy_paper_type)

			# Create order and associate to press
			# {"id"=>nil, "amount_in_pesewas"=>0,
			# "currency"=>"GHS", "state"=>"pending_confirmation",
			# "press_id"=>nil, "customer_id"=>nil, "press_name"=>nil,
			# "customer_name"=>nil, "uid"=>nil, "specs"=>nil,
			# "created_at"=>nil, "updated_at"=>nil, "is_invoice" => true}

			order = Order.new(currency: options[:currency],
												amount: 0.to_money(options[:currency]),
												press_id: press.id,
												customer_id: options[:customer].id,
												press_name: press.name,
												customer_name: options[:customer].uid || options[:customer].full_name,
												uid: generate_order_uid(options),
												specs: build_order_specs(params),
												state: "calculating_estimate")

			# if order is saved
			# create documents
			if order.save
				# save the delivery information associated with the order
				if(params[:delivery_location].present?)
					delivery = order.delivery
					delivery.update_attributes(location: params[:delivery_location],customer_id: order.customer_id, press_id: order.press_id)
				end

				# Associate order to country
				options[:customer].country.orders << order

				# Associate order to document
				params[:document_links].split("--|--").each do |document|
					result = save_document(order: order,params: {document: document}, customer: options[:customer])
					unless result[:success]
						order.complete!(false)
						return result
						break
					end
				end

				# Mark order as complete
				order.complete!
			else
				return {success: false, message: "We had an issue saving creating your order. Please try again."}
			end

			options[:order_id] = order.id

			# Calculate the estimated price
			# Resque.enqueue(CalculatorWorker,options.as_json)
			CalculatorWorker.perform_async(options.as_json)
			
			# Get delivery object
			delivery = order.delivery

			return {success: true, message: "Order successfully created.", data: {order: order.as_json, delivery: {location: delivery.location, state: delivery.status}}}
		else
			return {success: false, message: "No press available."}
		end
  end

  # Gives an estimated amount of how much
  # customer is supposed to pay based on the
  # number of pages contained in document
  def self.calculate_estimated_amount
  end

  # Generates a uid for
  # order being created
  def self.generate_order_uid(options)
  	country = options[:customer].country
  	return "PRD0#{country.next_order_id}"
  end

  # build the order specs
  # Based on the system being used
  # TODO: pick order specs from the db
  # and build according to who set the order
  def self.build_order_specs(params)
  	{ document: params[:service],
			paper_type: params[:paper_type],
			copy_paper_type: params[:copy_paper_type],
			make_copies: params[:has_copies] == "true" ? true : false,
			no_of_copies: params[:no_of_copies] || 0,
			service_type: params[:service_type],
			additional_info: params[:additional_info]
			}
  end

  # Saves document in db
  # and associate to order
  def self.save_document(options,has_order = true)
  	# Load document
  	params = options[:params]
  	document = params[:document]

  	#<Document id: nil, file_url: nil, filename: nil, is_archived: false, customer_id: nil, created_at: nil, updated_at: nil>
  	document_url, md5_hash = document.split("---~---")
  	document = options[:customer].documents.find_by_md5_hash(md5_hash)
  	if document.present?
  		if has_order
  			options[:order].documents << document
  		end
  		return {success: true, message: "Document processed -- #{document.filename}"}
  	end

  	document = Document.new(filename: get_file_name(document_url),
  													file_url: document_url,
  													md5_hash: md5_hash,
  													customer_id: options[:customer].id)

		if document.save
			# Calculate the metadata
			# in the background
			options[:document_id] = document.id
			# Resque.enqueue(MetadataWorker, options)

  		if has_order
  			options[:order].documents << document
  		end
  		return {success: true, message: "Document processed -- #{document.filename}"}
  	else
  		return {success: false, message: "Could not process document -- #{document.filename}"}
  	end
  end

  # Gets the file name
  def self.get_file_name(document)
  	document.split("/").last
  end

  # Calculates the price of the document
  def self.calculate_estimate_price(order)
  	# Pull the files from amazon
  	# Count pages and give user the amount they have to pay
  	# extensions not to pull [.jpg,.jpeg,.psd,.tiff,.png,.bmp]
  end

end