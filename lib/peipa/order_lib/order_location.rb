module Peipa::OrderLib::OrderLocation
  
  def self.process(options)
  		# Switch between libraries with respect to service
  		# params
		params = options[:params]


		# Structure reponse
		# Add time helper
		order = options[:customer].orders.find_by_id(params[:order_id])
		if order.present?
			customer_geo = options[:customer].gp
			document_geo = order.get_geolocation
			
			return { success: true, 
							 message: "Order found", 
							 data: { 
							 	document_geo: document_geo,
							 	customer_geo: {lat: customer_geo.lat, lng: customer_geo.lng}
								}
							}
		else
			return { success: false, message: "Order not found" }
		end
  end

end