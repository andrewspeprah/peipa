module Peipa::OrderLib::Engine
  
  def self.process(options)
  	#---------------------- VALIDATION ----------------------
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  return result unless result[:success]

	  # Get formatted msisdn
	  options[:formatted_msisdn] = result[:data][:msisdn]

  	# Customer
		options[:customer] = Customer.exclude(['password_digest']).find_by_msisdn(options[:formatted_msisdn])
		# Cancel if customer is not found
		return {success: false, message: "You are not registered on the platform. Please enter telephone number again."} unless options[:customer].present?

		# Check country and set currency
		result = Peipa::Util.get_currency(params)
		return result unless result[:success]
		options[:currency] = result[:data][:currency_code]

  	# Switch between libraries with respect to service
  	result =
	  	case options[:service]
	  	when "create"
	  		Peipa::OrderLib::CreateOrder.process(options)
	  	when "list"
	  		Peipa::OrderLib::List.process(options)
	  	when "location"
	  		Peipa::OrderLib::OrderLocation.process(options)
	  	when "press_confirm"
	  		Peipa::OrderLib::OrderLocation.process(options)
	  	when "customer_confirm"
	  		Peipa::OrderLib::OrderLocation.process(options)
	  	when "details"
	  		Peipa::OrderLib::OrderDetail.process(options)
	  	when "status"
	  		Peipa::OrderLib::Status.process(options)
	  	when "comments"
	  		Peipa::OrderLib::Comments.process(options)	  		
	  	end

	  return result
  end
end