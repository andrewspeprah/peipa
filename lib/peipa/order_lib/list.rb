module Peipa::OrderLib::List
  
  def self.process(options)
  		# Switch between libraries with respect to service
  		# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Customer
		customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)
		return {success: false, message: "You are not registered on the platform. Please enter telephone number again."} unless customer.present?

		# Structure reponse
		# Add time helper
		include ActionView::Helpers::DateHelper
		orders = []
		page = params[:page] || 1
		limit = params[:per_page] || 10
		paginate = params[:paginate] == "true" ? true : false

		# if customer is verified send customer's details
		if paginate
			return {success: true, message: "List of orders",
							data: {orders: 	customer.orders
																			.where("is_archived = false")
																			.order("created_at DESC")
																			.page(page)
																			.per(limit).map { |order|  format_json(order) }
										}
						 }
		else
			return {success: true, message: "List of orders",
							data: {orders: 	customer.orders
																	.where("lower(uid) LIKE ? AND is_archived = false","\%#{params[:query]}\%")
																	.order("created_at DESC").map { |order|  format_json(order) }
							}
			}
		end
	end

  # Format json for display
  def self.format_json(order)
		{currency: order.amount.currency.code,
		 amount: order.amount.to_f,
		 document: order.documents.pluck(:filename).join("\n"),
		 press: order.press.name,
		 moment_created: order.created_at.strftime("%d-%b-%Y %H:%M:%S"),
		 order_uid: order.uid,
		 state: order.state,
		 service_type: order.specs["service_type"]
		}
	end


  # Get the relative time for the day
  def self.relative_time(start_time)
	  diff_seconds = Time.now - start_time
	  case diff_seconds
	    when 0 .. 59
	      puts "#{diff_seconds} seconds ago"
	    when 60 .. (3600-1)
	      puts "#{diff_seconds/60} minutes ago"
	    when 3600 .. (3600*24-1)
	      puts "#{diff_seconds/3600} hours ago"
	    when (3600*24) .. (3600*24*30) 
	      puts "#{diff_seconds/(3600*24)} days ago"
	    else
	      puts start_time.strftime("%m/%d/%Y")
	  end
	end
end