module Peipa::OrderLib::Comments
  
  def self.process(options)
		# Switch between libraries with respect to service
		# params
		params = options[:params]

		# Structure reponse
		# Add time helper
		order = options[:customer].orders.find_by_uid(params[:order_uid])
		if order.present?
			return { success: true, 
							 message: "Order comments", 
							 data: { 
							 	comments: order.comments.select([:message,:from,:created_at])
							 }
						}
		else
			return { success: false, message: "Order not found" }
		end
  end

end