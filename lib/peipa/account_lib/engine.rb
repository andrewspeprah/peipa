module Peipa::AccountLib::Engine
  def self.process(options)
    # Switch between libraries with respect to service
    result =
      case options[:service]
      when "history"
        Peipa::AccountLib::History.process(options)
      when "payment"
        Peipa::AccountLib::Payment.process(options)
      when "callback"
        Peipa::AccountLib::Callback.process(options)
      else
        {success: false, message: "Request was not complete. Please try again"}
      end

    return result
  end
end
