module Peipa::AccountLib::History
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Customer
		customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)
		return {success: false, message: "Your account is not found on the system."} unless customer.present?

		page = params[:page] || 1
		limit = params[:per_page] || 10

		# Get account history
		acct_history = customer.account
													 .account_histories
													 .order("created_at DESC")
													 .page(page)
													 .per(limit)

		# if customer is present
		# send account history
		return {success: true,
						message: "Account History",
						data: {
							account_histories: acct_history.map { |e| {created_at: e.created_at.strftime("%d-%b-%Y %H:%M:%S %p"), 
																												trans_type: e.transaction_type,
																												amount: "#{e.amount.currency.code}#{e.amount.to_f}",
																												service_type: e.service.titleize,
																												remarks: e.remarks}
																									}
						}
					}
	end
end