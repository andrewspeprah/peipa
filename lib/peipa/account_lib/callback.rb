module Peipa::AccountLib::Callback
  # Saves customers details and starts verification
  def self.process(options)
    # params
    params = options[:params]

    # Find transaction
    # callback to be received
    transaction = Transaction.where(status: "pending", reference: params[:reference])

    # If the transaction is not present
    # return the callback
    unless transaction.present?
      return {success: false, message: "Transaction not found"}
    end

    # If transaction is present
    # Update transaction
    transaction = transaction.first
    transaction.update_attributes(status: params[:status], response: params.to_json)
    customer = transaction.customer

    # Build Message
    status = params[:status].downcase
    if status == "paid"
      # Credit the customers account
      customer.account.credit!({amount: params[:amount].to_money * 100,
                                service: "mobile_money_topup",
                                remarks: "Voucher topup of GHS#{params[:amount]} from #{params[:mobile]}"})

      message = "Topup of GHS#{params[:amount]} successful."
    elsif status == "failed"
      message = "Sorry, your payment was not successful. Please try again later."
    else
      if params[:message].present?
        message = params[:message].split("|")[0].titleize
      else
        message = "Sorry, your payment could not be processed. Please try again later."
      end
    end

    # Notify the user
    options = {country_id: customer.country.id,
               user_id: customer.id,
               user_type: "customer",
               message: message}

    # Send the notification
    # Resque.enqueue(FcmWorkers::NotifyWorker, options)
    NotifyWorker.perform_async(options)    
  end
end
