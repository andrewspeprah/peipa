module Peipa::AccountLib::Payment
  # Saves customers details and starts verification
  def self.process(options)
    # params
    params = options[:params]

    # Convert msisdn
    # Use util to validate phone
    result = Peipa::Util.validate_phone(options)
    unless result[:success]
      return result
    end
    formatted_msisdn = result[:data][:msisdn]

    # Customer
    customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)
    return {success: false, message: "Your account is not found on the system."} unless customer.present?

    # Check whether customer has fully registered their information
    unless customer.full_name.present? && customer.location_address.present?
      return {success: false, message: "Please complete your profile information before performing payment."}
    end

    # Convert the phone number here into international version
    un_locode = ISO3166::Country.find_country_by_name(customer.country.name).un_locode
    phonelib_obj = Phonelib.parse(params[:paymentMsisdn], un_locode)
    international_msisdn = phonelib_obj.international(false)

    # If the user have their information in the system, then we can process payment
    #{
    #	"AppCode": "peipa",
    #	"RequestId":"LIVE00013",
    #	"Reference": "LIVE00013",
    #	"Amount": "1",
    #	"Currency": "GHS",
    #	"TransactionType": "MMCredit",
    #	"Msisdn": "+233556158311",
    #	"Country": "Ghana",
    #	"Name": "Andrews Peprah",
    #	"Email": "andrewspeprah@gmail.com",
    #	"Remarks": "Transaction Test",
    #	"Test": false,
    #	"PayMethod": "interpay_smartkode"
    #}

    # Generate reference
    reference = "PP#{Time.now.to_i}"

    carrier = params[:carrier] || phonelib_obj.carrier
    # Create a transaction object
    transaction = Transaction.new(amount_in_pesewas: (params[:amount].to_f * 100),
                                  current_balance_in_pesewas: (customer.account.balance.to_f * 100),
                                  provider: carrier,
                                  status: "pending",
                                  msisdn: international_msisdn,
                                  reference: reference,
                                  provider_reference: reference,
                                  service: "MMDebit",
                                  params: {voucher_code: params[:voucher]},
                                  customer_id: customer.id,
                                  remarks: "Voucher topup by Mobile Money. #{international_msisdn}")

    # Save transaction
    if transaction.save
      # Create Payment model for storing payment
      # requests performed by customers
      url = ENV['payment_endpoint']
      response = HTTP.post(url, :json => {AppCode: "peipa",
                                          RequestId: "RID#{reference}",
                                          Reference: reference,
                                          Amount: params[:amount],
                                          Currency: "GHS",
                                          TransactionType: "MMDebit",
                                          Msisdn: international_msisdn,
                                          Country: "Ghana",
                                          Carrier: carrier,
                                          Name: customer.full_name,
                                          Email: customer.email,
                                          Remarks: "Withdrawal from wallet #{international_msisdn}",
                                          Test: true,
                                          CardNumber: params[:voucher],
                                          PayMethod: "itc_smartkode"})

      # Save the response in the database
      transaction.update_attribute(:response, response)

      # Check if the payment is successful, then send the correct response to the user
      if response[:status_code] == "1"
        return {success: true, message: "Payment processed. Please complete payment on your mobile money number."}
      end
    end

    # If transaction does not succeed, then it definitely failed
    return {success: true, message: "Your payment request is being processed."}
  end
end
