module Peipa::Util
  
  def self.validate_phone(options)
  	options = options[:params]

  	# Get country code
	  country_object = ISO3166::Country.find_country_by_name(options[:country])
	  return {success: false, message: "Invalid country provided"} unless country_object.present?
	  
	  country_code = country_object.gec

		# Validate msisdn then perform transaction
	 	formatted_msisdn = Phonelib.parse(options[:msisdn], country_code)
	  
	  # If invalid return error
	  if formatted_msisdn.invalid?
	    return {success: false, message: "Invalid phone number. Please try again."}
	  end

	  # convert to string
	  return {success: true, message: "Valid phone number", data: {msisdn: formatted_msisdn.international(false)}}
  end

  # Returns currency for country
  def self.get_currency(options)
  	# Get country code
	  country_object = ISO3166::Country.find_country_by_name(options[:country])
	  if country_object.present?
	  	return {success: true, message: "Currency found.", data: {currency_code: country_object.currency.code}}
	  else
	  	return {success: false, message: "Invalid currency. Currency not found"}
	  end
  end
end