module Peipa::VoucherLib::Verify
  
  def self.process(options)
    # params
	params = options[:params]

    # Encrypt voucher code
    encrypted_code = Encryption.encrypt(options[:voucher_code])

    # Find voucher code
    voucher = country.vouchers.find_by_number(encrypted_code)

    # if voucher is not present
    # send a warning
    if voucher.present?
    	return {success: true, message: "Voucher is valid and present."}
    else
        return {success: false, message: "Voucher not present."}
    end
  end
end