module Peipa::VoucherLib::Engine
  
  def self.process(options)
  	result =
	  	case options[:service]
	  	when "generate"
	  		Peipa::VoucherLib::Generate.process(options)
	  	when "topup"
	  		Peipa::VoucherLib::Topup.process(options)
	  	end

	  return result
  end
end