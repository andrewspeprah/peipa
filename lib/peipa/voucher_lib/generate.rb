module Peipa::VoucherLib::Generate
  
  def self.process(options)
    # params
	voucher_code = sprintf('%012d', rand(10**12))

    # Get the currency
    result = Peipa::Util.get_currency(country: options[:country])
    return result unless result[:success]
    currency = result[:data][:currency_code]

    # Get country
    country = Country.find_by_name(options[:country])
    unless country.present?
    	return {success: false, message: "Country not found"}
    end

    if country.vouchers.create(number: voucher_code,
								amount: options[:amount].to_f,
								currency: currency,
								expires_at: (Time.now + 3.months))
    	return {success: true, message: "Voucher created"}
    else
    	process(options)
    end
  end
end