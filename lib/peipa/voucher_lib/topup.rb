module Peipa::VoucherLib::Topup
  
  def self.process(options)
    # params
		params = options[:params]

    # Find voucher code
    country = Country.find_by_name(params[:country])

    unless country.present?
    	return {success: false, message: "Invalid country entered. Please try again."}
    end
    
    voucher = country.vouchers.find_by_number(params[:voucher_code])

    # if voucher is not present
    # send a warning
    unless voucher.present?
    	return {success: false, message: "Invalid voucher code. Please check and try again. Several wrong attempts could block your account."}
    end

		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

	  # Step 1: validate customer's existense on system
	  # and send validation sms / email
	  customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)
	  unless customer
	  	return {success: false, message: "Your details are not on the system."}
	  end

    # Get voucher amount
    # and topup customer's account
    return customer.account.topup(voucher)
  end
end