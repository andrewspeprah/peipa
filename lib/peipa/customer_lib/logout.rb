module Peipa::CustomerLib::Logout
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Customer
		customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)

		if customer.present?
			customer.logout!
			return {success: true, message: "Device disabled from receiving FCM"}
		end
	end
end