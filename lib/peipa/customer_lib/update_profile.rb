module Peipa::CustomerLib::UpdateProfile
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Customer
		customer = Customer.find_by_msisdn(formatted_msisdn)

		if customer.present?
			# Update customer's details
			# and return response
			if customer.update_attributes(ppt_pic: params[:ppt_pic] || customer.ppt_pic, 
																		full_name: params[:full_name] || customer.full_name,
																		location_address: params[:location_address] || customer.location_address,
																		email: params[:email] || customer.email)
				return Peipa::CustomerLib::Registration.format_customer_response("Profile successfully updated",customer,true)
			else
				return Peipa::CustomerLib::Registration.format_customer_response("Couldn't save your profile. Please try again",customer,false)
			end
		else
			return {success: false, message: "Your profile is not found on the system."}
		end
	end
end