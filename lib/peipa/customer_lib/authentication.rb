module Peipa::CustomerLib::Authentication
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

	  # find country
	  country = Country.find_by_name(params[:country])
	  return {success: false, message: "Country not found"} unless country.present?

		# Customer
		customer = country.customers.find_by_msisdn(formatted_msisdn)

		# Cancel if customer is not found
		unless customer.present?
			return {success: false, message: "You are not registered on the platform. Please enter telephone number again."}
		end

		# if customer is verified send customer's details
		case options[:service]
		when "authentication"
			if customer.authenticate(params[:pin_code])
				return Peipa::CustomerLib::Registration.format_customer_response("PIN code correct.",customer,true)
			else
				return {success: false, message: "Wrong PIN. Please try again"}
			end
		when "pin_change"
			if customer.pin_set
				if customer.authenticate(params[:current_pin])
					set_pin(customer,params)	
				else
					return {success: false, message: "Wrong PIN. Please try again"}
				end
			else
				set_pin(customer,params)
			end
		end
  end

  # Sets customer's pin
  def self.set_pin(customer,params)
  	if customer.update_attributes(pin_set: true, password: params[:pin_code])
					return Peipa::CustomerLib::Registration.format_customer_response("PIN code successfully changed.",customer,true)
		else
			return {success: false, message: "There was an issue setting your PIN. Please try again."}
		end
  end
end