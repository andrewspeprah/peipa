module Peipa::CustomerLib::Profile
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Customer
		customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)

		if customer.present?
			return Peipa::CustomerLib::Registration.format_customer_response("Customer profile",customer,true)
		end
	end
end