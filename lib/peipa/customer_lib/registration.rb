module Peipa::CustomerLib::Registration
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

	  # Step 0: check country's availability
	  country = Country.find_by_name(params[:country])
	  return {success: false, message: "Your country is currently not available. Please contact Peipa Support for more enquiries."} unless country.present?

	  # Step 1: validate customer's existense on system
	  # and send validation sms / email
	  customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)

	  # If customer is found
	  if customer.present?
	  	customer.device.enable!
	    # Verify the device being used
	    # for the registration whether is
	    # the same device or not
	    if customer.device.try(:uid) == params[:imei].strip
	    	# that means user is using the same
	    	# device
	    	# hence let user login if verified
	    	if customer.is_verified
	    		# Enable device for user to receive notifications
	    		return format_customer_response("Welcome to Peipa",customer,true)
	    	else
	    		result = Peipa::SmsLib::Verification.send_verification(user: customer)
	    		return Peipa::CustomerLib::Registration.format_customer_response(result[:message],customer,result[:success])
	    	end
	    else
	    	# Save device against customer
	    	customer.device = Device.create(uid: params[:imei])
				unless customer.is_verified
					# Send a verification code to customer to verify themselve
					result = Peipa::SmsLib::Verification.send_verification(user: customer)
				end
	    	return Peipa::CustomerLib::Registration.format_customer_response(result[:message],customer,result[:success])
	    end
	  else
	  	# Generate random UID for customers
	  	customer_uid = "PC#{sprintf('%06d', rand(10**6))}"
	  	loop do
	  		customer_uid = "PC#{sprintf('%06d', rand(10**6))}"
	  		break unless Customer.find_by_uid(customer_uid).present?
	  	end

	    # If customer is not found
	    # create customer in db
	    # and send a validation sms for input
	    customer = Customer.new(msisdn: formatted_msisdn,
	    								location_address: "",
	    								email: "",
	    								uid: customer_uid,
	    								password: customer_uid,
	    								password_confirmation: customer_uid)

	    # Save device details
	    if customer.save
	    	# Assign device to customer
	    	customer.device = Device.create(uid: params[:imei])
	    	
	    	# Assign country to customer
	    	customer.country = country

	    	customer.account
	    	result = Peipa::SmsLib::Verification.send_verification(user: customer)

	    	# Format response
	    	# After sms has been sent to customer
	      if result[:success]
	        return format_customer_response("Verification SMS sent.",customer,true)
	      else
	        return format_customer_response("Couldn't send SMS verification. Please try again.",customer)
	      end

	    else
	    	return {success: false, message: "We had some issues saving your details. Please try again."}
	    end
	  end
	end

	# Format response
	# of customer
	def self.format_customer_response(message,customer,success = false)
		return {success: success,
	    						message: message,
	    						data: {customer: {
	    											full_name: customer.full_name || " ",
	    											msisdn: customer.msisdn,
	    											country: customer.country.try(:name) || " ",
	    											is_active: customer.is_active,
	    											email: customer.email,
	    											location_address: customer.location_address,
	    											uid: customer.uid,
	    									 		is_verified: customer.is_verified,
	    									 		pin_set: customer.pin_set,
	    									 		ppt_pic: customer.ppt_pic,
	    									 		created_at: customer.created_at
	    												},
	    									 account: {
	    									 						balance: '%.2f' % customer.account.balance.to_f,
	    									 						currency: customer.account.balance.currency.code
	    									 					}
	    									}
	    								}
	end
end