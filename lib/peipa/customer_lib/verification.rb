module Peipa::CustomerLib::Verification
	# Saves customers details and starts verification
	def self.process(options)
		# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Customer
		customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)

		# Cancel if customer is not found
		unless customer.present?
			return {success: false, message: "You are not registered on the platform. Please enter telephone number again."}
		end

		# if customer is verified send customer's details
		if customer.is_verified
			return Peipa::CustomerLib::Registration.format_customer_response("Welcome to Peipa",customer,true)
		end

		# Verify code
		verification_log = customer.verification_logs.order("created_at DESC").first

		# if verification code is not present
		# send wrong verification code back
		# and give retry times
		# TODO: set retry times for verification
		unless verification_log.present?
			return Peipa::CustomerLib::Registration.format_customer_response("Wrong verification code. Please try again.",customer,false)
		else
			unless verification_log.code == params[:code].strip
				return Peipa::CustomerLib::Registration.format_customer_response("Wrong verification code. Please try again.",customer,false)
			end
		end


		# if verification log is present
		# set customer as verified and send verification result
		if customer.update_attribute(:is_verified, true)
			verification_log.update_attribute(:is_verified, true)
			return Peipa::CustomerLib::Registration.format_customer_response("Verification complete. Welcome to Peipa",customer,true)
		else
			return Peipa::CustomerLib::Registration.format_customer_response("Verification failed. Please try again",customer,false)
		end
	end
end