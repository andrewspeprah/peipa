module Peipa::CustomerLib::Engine
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	result =
	  	case options[:service]
	  	when "registration"
	  		Peipa::CustomerLib::Registration.process(options)
	  	when "verification"
	  		Peipa::CustomerLib::Verification.process(options)
	  	when "authentication","pin_change"
	  		Peipa::CustomerLib::Authentication.process(options)
	  	when "profile"
	  		Peipa::CustomerLib::Profile.process(options)
	  	when "update_profile"
	  		Peipa::CustomerLib::UpdateProfile.process(options)
	  	when "logout"
	  		Peipa::CustomerLib::Logout.process(options)
	  	else
	  		{success: false, message: "Request was not complete. Please try again"}
	  	end

	  return result
  end
end