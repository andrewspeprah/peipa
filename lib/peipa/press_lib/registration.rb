module Peipa::PressLib::Registration
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end

	  formatted_msisdn = result[:data][:msisdn]

	  # TODO: validate country name
	  country = Country.find_by_name(params[:country])
	  return {success: false, message: "Country currently not active."} unless country.present?

  	# Generate random UID for customers
  	press_uid = "PP#{sprintf('%06d', rand(10**6))}"
  	loop do
  		press_uid = "PP#{sprintf('%06d', rand(10**6))}"
  		break unless Press.find_by_uid(press_uid).present?
  	end

		# Get presses of which customer
		# belongs to
		# name: nil, info: nil, location_address: nil,
		# telephone: nil, is_active: true, is_verified: false
		press = country.presses.new(name: params[:name],
																info: params[:info],
																location_address: params[:location_address],
																telephone: params[:telephone],
																msisdn: formatted_msisdn,
																uid: press_uid,
																country_id: country.id)

		# After saving press
		# Send an sms to verify the press
		if press.save
			press.country = country
			gp = Gp.new(lat: params[:lat], lng: params[:lng], name: params[:name], country: country.name)
			if gp.save
				press.gp = gp

				# After save send a verification code to press number
				options[:user] = press
				result = Peipa::SmsLib::Verification.send_verification(options)

				# On receiving the result 
				# format message and send to customer
				if result[:success]
	        return render_success("Thank you for registering with Peipa. Please enter verification code",press,true)
	      else
	        return render_success("Couldn't send SMS verification. Please try again.",press)
	      end
			else
				return render_error
			end
		else
			return render_error
		end
  end

  def self.render_error
  	{success: false, message: "Error setting up your account. Please try again."}
  end

  def self.render_success(message,press,success = false)
  	return {success: success, message: message,
							data: {
							press: {
									id: press.id || "",
									name: press.name || "",
									info: press.info || "",
									location_address: press.location_address || "",
									telephone: press.telephone || "",
									is_active: press.is_active,
									is_verified: press.is_verified,
									created_at: press.created_at,
									updated_at: press.updated_at,
									msisdn: press.msisdn,
									uid: press.uid || "",
									pic_url: press.pic_url,
									location_pic_url: press.location_pic_url,
									is_approved: press.is_approved,
									country: press.country.name
								},
							account: {
								balance: press.account.balance.to_f,
								currency: press.account.balance.currency.code
							}
							}
				}
  end
end