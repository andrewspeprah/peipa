module Peipa::PressLib::Service
  
  def self.process(options)
  	result = 
	  	case options[:service].split("|")[1]
	  	when "registration"
	  		register_service(options)
	  	when "paper_type_creation"
	  		create_paper_type(options)
	  	end

	  return result
  end

  # creates a new service for the 
  # printing press
  def self.register_service(options)
  	params = options[:params]

  	service = options[:press].services.new(name: params[:name],
  																				 description: params[:description],
  																				 is_active: true)
  	if service.save
  		return {success: true, message: "You've successfully created the service.", data: {service: service}}
  	else
  		return {success: false, message: "An error occured while saving the service. Please make sure the service has a unique name and try again", data: {service: service}}
    end
  end

  # Creates a new paper type for the service
  # selected for the customer
  def self.create_paper_type(options)
  	params = options[:params]

  	service = options[:press].services.find_by_id(options[:service_id])
  	return {success: false, message: "Service not found."} unless service.present?

  	# New Paper params
  	# {"id"=>nil, "size"=>nil, "ptype"=>nil, "color"=>nil, "currency"=>nil, "unit"=>nil, "service_id"=>nil, "created_at"=>nil, "updated_at"=>nil, "price_in_pesewas"=>0}
  	paper = service.papers.new(size: params[:size],
  														 ptype: params[:ptype],
  														 color: params[:color],
  														 currency: params[:currency],
  														 unit: params[:unit],
  														 price: params[:price].to_money)

  	if paper.save
  		return {success: true, message: "Paper successfully saved", data: {paper: paper}}
  	else
  		return {success: false, message: "An error occured while saving the paper type. Please make sure the paper has a unique name and try again", data: {paper: paper}}
  	end
  end

end