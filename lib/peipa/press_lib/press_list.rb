module Peipa::PressLib::PressList
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Customer
		customer = Customer.exclude(['password_digest']).find_by_msisdn(formatted_msisdn)

		# Cancel if customer is not found
		unless customer.present?
			return {success: false, message: "You are not registered on the platform. Please enter telephone number again."}
		end

		# Get presses of which customer
		# belongs to
		presses = customer.country.try(:presses)
		# current_location = Geokit::LatLng.new(37.79363,-122.396116)
		# destination = "37.786217,-122.41619"
		# current_location.distance_to(destination)

		# if customer is verified send customer's details
		if presses.available.present?
			page = params[:page] || 1
			limit = params[:per_page] || 10
			query = params[:query].try(:downcase)

			return {success: true, message: "Welcome to Peipa",
							data: {presses: presses.available
																		.where("lower(name) LIKE ? OR lower(location_address) LIKE ?","\%#{query}\%","\%#{query}\%")
																		.order("created_at DESC")
																		.page(page)
																		.per(limit)
									}
								}
		else
			return {success: false, message: "No printing presses available."}
		end
  end
end