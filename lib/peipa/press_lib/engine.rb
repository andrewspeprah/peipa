module Peipa::PressLib::Engine
  
  def self.process(options)
  	# Search for press if
  	# service is not registration
  	unless ["list","register"].include? options[:service]
  		# Find country
  		params = options[:params]
			# Convert msisdn
			# Use util to validate phone
		  result = Peipa::Util.validate_phone(options)
		  return result unless result[:success]
		  formatted_msisdn = result[:data][:msisdn]
  		
  		# Find country
  		options[:country] = Country.find_by_name(params[:country])
  		return {success: false, message: "Selected country is unavailable"} unless options[:country].present?

  		# Get press with formatted msisdn
  		options[:press] = options[:country].presses.find_by_id(params[:press_id])
  		return {success: false, message: "Printing press not pressent"} unless options[:press].present?
  	end

  	# Switch between libraries with respect to service
  	result =
	  	case options[:service]
	  	when "list"
	  		Peipa::PressLib::PressList.process(options)
	  	when "profile"
	  		Peipa::PressLib::PressProfile.process(options)
	  	when "register"
	  		Peipa::PressLib::Registration.process(options)
	  	when "service|registration"
	  		Peipa::PressLib::Service.process(options)
      when "service|paper_type_creation"
        Peipa::PressLib::Service.process(options)
	  	end

	  return result
  end
end