module Peipa::PressLib::PressProfile
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# params
		params = options[:params]

		# Convert msisdn
		# Use util to validate phone
	  result = Peipa::Util.validate_phone(options)
	  unless result[:success]
	  	return result
	  end
	  formatted_msisdn = result[:data][:msisdn]

		# Customer
		customer = Customer.find_by_msisdn(formatted_msisdn)

		# Cancel if customer is not found
		unless customer.present?
			return {success: false, message: "You are not registered on the platform. Please enter telephone number again."}
		end

		# Get presses of which customer
		# belongs to
		press = customer.country.presses.find_by_id(params[:press_id])
		# current_location = Geokit::LatLng.new(37.79363,-122.396116)
		# destination = "37.786217,-122.41619"
		# current_location.distance_to(destination)


		# if customer is verified send customer's details
		if press.present?
			services = []
			press.services.each do |service|
				services << {service: service, doctypes: service.papers}
			end

			return {success: true, message: "Welcome to Peipa",
							data: {press: press,
										services: services
									}
								}
		else
			return {success: false, message: "No press available."}
		end
  end
end