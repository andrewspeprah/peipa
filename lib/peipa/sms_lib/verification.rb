module Peipa::SmsLib::Verification
  
  def self.process(options)
  	# Switch between libraries with respect to service
  	# TODO: Move to worker
  	result = HTTP.post(ENV['sms_endpoint'], :params => {to: options[:msisdn], msg: options[:message]})

  	if result.to_s
  		# Save sms log in db
      status = "successful"
      success = true
      message = "Message sent to #{options[:msisdn]}"
    else
      status = "failed"
      success = false
      message = "There was an error sending message - Error code: #{result.to_s}"
    end
    
    # Create an sms log
    SmsLog.create(recipient: options[:msisdn],
                  message: options[:message],
                  service: options[:service],
                  status: status,
                  status_code: result.to_s)
    
    return {success: success, message: message}
  end

  # Sends verification sms to customer
  def self.send_verification(options)
    # Create verification code
    verification_code = sprintf('%04d', rand(10 ** 4))
    verification_log = VerificationLog.new(code: verification_code,
                                           is_verified: false)

    if verification_log.save
      # Assing verification log to user
      options[:user].verification_logs << verification_log

      # Send sms to customer
      options[:message] = "Your Peipa verification code #{verification_code}"
      options[:msisdn] = options[:user].msisdn
      options[:service] = "verification"
      
      # Process SMS and send result
      return process(options)
    else
      return {success: false, message: "Error while saving verification code."}
    end
  end


end