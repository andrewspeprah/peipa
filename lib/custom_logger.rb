# lib/custom_logger.rb
class CustomLogger < Logger
  def format_message(severity, timestamp, progname, msg)
    "#{timestamp.to_formatted_s(:db)} #{severity} #{msg}\n"
  end
end

logfile = File.open("#{Rails.root}/log/telco_response.log", 'a')  # create log file
logfile.sync = true  # automatically flushes data to file
TELCO_RESPONSES = CustomLogger.new(logfile)  # constant accessible anywhere

logfile = File.open("#{Rails.root}/log/error_transactions.log", 'a')  # create log file
logfile.sync = true  # automatically flushes data to file
ERROR_TRANSACTION_LOG = CustomLogger.new(logfile)  # constant accessible anywhere
