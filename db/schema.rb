# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180328031201) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_histories", force: :cascade do |t|
    t.integer "amount_in_pesewas"
    t.string "service"
    t.string "remarks"
    t.integer "transaction_id"
    t.integer "account_id"
    t.integer "new_balance_in_pesewas", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "currency", default: "GHS"
    t.string "transaction_type", default: ""
    t.json "params", default: {}
    t.index ["id"], name: "index_account_histories_on_id"
  end

  create_table "accounts", force: :cascade do |t|
    t.bigint "balance_in_pesewas", default: 0
    t.boolean "is_active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "currency", default: "GHS"
  end

  create_table "app_specs", force: :cascade do |t|
    t.integer "client_id"
    t.json "other_params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "client_id"], name: "index_app_specs_on_id_and_client_id"
  end

  create_table "charge_logs", force: :cascade do |t|
    t.integer "order_id"
    t.integer "gross_amount_in_pesewas"
    t.integer "charged_ammount_in_pesewas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "order_id"], name: "index_charge_logs_on_id_and_order_id"
  end

  create_table "charge_specs", force: :cascade do |t|
    t.integer "amount_in_pesewas"
    t.integer "fee_in_pesewas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "currency", default: "GHS"
    t.integer "country_id"
    t.index ["id"], name: "index_charge_specs_on_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.boolean "is_active", default: true
    t.boolean "is_archived", default: false
    t.json "other_params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_clients_on_id"
  end

  create_table "collection_logs", force: :cascade do |t|
    t.string "category", default: "press_customer"
    t.string "remarks", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order_id"
    t.string "request_type", default: "customer to press"
  end

  create_table "comments", force: :cascade do |t|
    t.text "message"
    t.boolean "is_archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "from", default: ""
    t.index ["id"], name: "index_comments_on_id"
  end

  create_table "company_profiles", force: :cascade do |t|
    t.integer "company_id"
    t.integer "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "company_id"], name: "index_company_profiles_on_id_and_company_id"
  end

  create_table "confirmation_logs", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "press_id"
    t.integer "amount_in_pesewas", default: 0
    t.boolean "is_approved", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.boolean "is_active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_countries_on_id"
  end

  create_table "country_customers", force: :cascade do |t|
    t.integer "country_id"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "country_id"], name: "index_country_customers_on_id_and_country_id"
  end

  create_table "country_orders", force: :cascade do |t|
    t.integer "country_id"
    t.integer "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "country_id", "order_id"], name: "index_country_orders_on_id_and_country_id_and_order_id"
  end

  create_table "country_presses", force: :cascade do |t|
    t.integer "country_id"
    t.integer "press_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "country_id"], name: "index_country_presses_on_id_and_country_id"
  end

  create_table "country_runners", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_accounts", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_devices", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "device_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_gps", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "gp_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "customer_id", "gp_id"], name: "index_customer_gps_on_id_and_customer_id_and_gp_id"
  end

  create_table "customer_profiles", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "customer_id"], name: "index_customer_profiles_on_id_and_customer_id"
  end

  create_table "customer_verification_logs", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "verification_log_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string "full_name", default: " "
    t.string "msisdn"
    t.text "location_address"
    t.boolean "is_active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.string "uid"
    t.boolean "is_verified", default: false
    t.string "password_digest"
    t.integer "country_id"
    t.integer "client_id"
    t.boolean "pin_set", default: false
    t.string "ppt_pic", default: ""
  end

  create_table "deliveries", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "runner_id"
    t.integer "press_id"
    t.string "status", default: "pending_collection"
    t.boolean "is_delivered", default: false
    t.integer "amount_in_pesewas", default: 0
    t.integer "commission_in_pesewas", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order_id"
    t.text "location", default: ""
    t.index ["id", "customer_id"], name: "index_deliveries_on_id_and_customer_id"
  end

  create_table "devices", force: :cascade do |t|
    t.string "uid"
    t.boolean "is_active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "fcm_id", default: " "
  end

  create_table "documents", force: :cascade do |t|
    t.text "file_url"
    t.string "filename"
    t.boolean "is_archived", default: false
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "md5_hash"
    t.integer "page_count", default: 0
    t.boolean "is_calculated", default: false
    t.index ["id", "customer_id"], name: "index_documents_on_id_and_customer_id"
  end

  create_table "extra_charges", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "amount_in_pesewas"
    t.string "currency"
    t.boolean "is_accepted", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gps", force: :cascade do |t|
    t.string "lat"
    t.string "lng"
    t.string "name"
    t.string "country"
    t.string "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_gps_on_id"
  end

  create_table "metadata", force: :cascade do |t|
    t.integer "document_id"
    t.json "params", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_comments", force: :cascade do |t|
    t.integer "order_id"
    t.integer "comment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_order_comments_on_id"
  end

  create_table "order_documents", force: :cascade do |t|
    t.integer "order_id"
    t.integer "document_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "order_id"], name: "index_order_documents_on_id_and_order_id"
  end

  create_table "order_extracharges", force: :cascade do |t|
    t.integer "order_id"
    t.integer "extra_charge_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id"], name: "index_order_extracharges_on_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "amount_in_pesewas", default: 0
    t.string "currency", default: "GHS"
    t.string "state", default: "pending_confirmation"
    t.integer "press_id"
    t.integer "customer_id"
    t.string "press_name"
    t.string "customer_name", default: " "
    t.string "uid"
    t.json "specs"
    t.text "additional_info", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_invoice", default: true
    t.boolean "is_complete", default: false
    t.boolean "is_estimated", default: false
    t.integer "copies_amount_in_pesewas", default: 0
    t.integer "no_of_sheets", default: 0
    t.boolean "is_archived", default: false
    t.string "archive_flags", default: "0,0,0"
    t.boolean "is_viewed", default: false
    t.index ["id", "customer_id"], name: "index_orders_on_id_and_customer_id"
  end

  create_table "papers", force: :cascade do |t|
    t.string "size"
    t.string "ptype"
    t.string "color"
    t.string "currency"
    t.string "unit"
    t.integer "service_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "price_in_pesewas", default: 0
    t.index ["id", "service_id"], name: "index_papers_on_id_and_service_id"
  end

  create_table "press_accounts", force: :cascade do |t|
    t.integer "press_id"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "press_devices", force: :cascade do |t|
    t.integer "press_id"
    t.integer "device_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "press_gps", force: :cascade do |t|
    t.integer "press_id"
    t.integer "gp_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "press_id", "gp_id"], name: "index_press_gps_on_id_and_press_id_and_gp_id"
  end

  create_table "press_runners", force: :cascade do |t|
    t.integer "press_id"
    t.integer "runner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "press_users", force: :cascade do |t|
    t.integer "press_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "press_verification_logs", force: :cascade do |t|
    t.integer "press_id"
    t.integer "verification_log_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "presses", force: :cascade do |t|
    t.string "name", default: "--"
    t.string "info", default: "--"
    t.string "location_address", default: "--"
    t.string "telephone", default: "--"
    t.boolean "is_active", default: true
    t.boolean "is_verified", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "msisdn"
    t.integer "country_id", default: 0
    t.string "uid"
    t.string "pic_url", default: ""
    t.string "location_pic_url", default: ""
    t.boolean "is_approved", default: false
  end

  create_table "product_logs", force: :cascade do |t|
    t.string "msisdn"
    t.string "country"
    t.integer "product_id"
    t.text "remarks"
    t.text "raw_log"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "size"
    t.string "unit_price"
    t.string "expiry_date"
    t.string "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "picture_url"
    t.string "category"
    t.string "sell_price"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "pic_url", default: ""
    t.string "location_pic_url", default: ""
    t.text "details", default: ""
    t.json "params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.json "privileges"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "runner_accounts", force: :cascade do |t|
    t.integer "runner_id"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "runner_devices", force: :cascade do |t|
    t.integer "runner_id"
    t.integer "device_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "runner_gps", force: :cascade do |t|
    t.integer "runner_id"
    t.integer "gp_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "runner_id", "gp_id"], name: "index_runner_gps_on_id_and_runner_id_and_gp_id"
  end

  create_table "runners", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.text "location_address"
    t.string "msisdn"
    t.boolean "is_active", default: true
    t.boolean "is_verified", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pic_url", default: ""
    t.string "location_pic_url", default: ""
    t.string "password_digest"
    t.integer "country_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
    t.boolean "is_active"
    t.string "description"
    t.json "params"
    t.integer "press_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id", "press_id"], name: "index_services_on_id_and_press_id"
  end

  create_table "shops", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "tag_line"
    t.string "telephone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sms_logs", force: :cascade do |t|
    t.string "recipient", default: ""
    t.text "message"
    t.string "service"
    t.string "status"
    t.string "status_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.integer "amount_in_pesewas", default: 0
    t.integer "current_balance_in_pesewas", default: 0
    t.string "provider"
    t.string "status"
    t.string "msisdn"
    t.string "reference"
    t.string "provider_reference"
    t.string "service"
    t.json "params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "remarks"
    t.text "response"
    t.integer "customer_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.integer "role_id"
    t.string "role_name", default: "staff"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "verification_logs", force: :cascade do |t|
    t.string "code"
    t.boolean "is_verified", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vouchers", force: :cascade do |t|
    t.text "number", default: ""
    t.datetime "expires_at"
    t.integer "amount_in_pesewas", default: 0
    t.string "currency", default: "GHS"
    t.boolean "is_used", default: false
    t.integer "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_printed", default: false
    t.index ["id", "country_id"], name: "index_vouchers_on_id_and_country_id"
  end

end
