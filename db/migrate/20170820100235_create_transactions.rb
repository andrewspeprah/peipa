class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
    	t.integer	:amount_in_pesewas, default: "00"
    	t.integer	:current_balance_in_pesewas, default: "00"
    	t.string	:provider
    	t.string	:status
    	t.string	:msisdn
    	t.string	:reference
    	t.string	:provider_reference
      t.string  :service
    	t.column	:params, :json
      t.timestamps
    end
  end
end
