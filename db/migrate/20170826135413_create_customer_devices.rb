class CreateCustomerDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_devices do |t|
    	t.integer		:customer_id
    	t.integer 	:device_id
      t.timestamps
    end
  end
end
