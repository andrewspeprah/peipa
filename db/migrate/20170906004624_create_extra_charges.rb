class CreateExtraCharges < ActiveRecord::Migration[5.1]
  def change
    create_table :extra_charges do |t|
    	t.string 	 	:name
    	t.text 			:description
    	t.integer		:amount_in_pesewas
    	t.string 		:currency
    	t.boolean		:is_accepted, default: false
      t.timestamps
    end
  end
end
