class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
    	t.string 	 :name
    	t.text 		 :description
    	t.boolean  :is_active, default: true
    	t.boolean  :is_archived, default: false
    	t.column   :other_params, :json
      t.timestamps
    end

    add_index :clients, [:id]
  end
end
