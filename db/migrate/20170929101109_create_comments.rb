class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
    	t.text 		 	:message
    	t.boolean 	:is_archived, default: false
      t.timestamps
    end

    add_index :comments, [:id]
  end
end
