class CreateCountryCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :country_customers do |t|
    	t.integer	 :country_id
    	t.integer	 :customer_id
      t.timestamps
    end

    add_index :country_customers, [:id,:country_id]
  end
end
