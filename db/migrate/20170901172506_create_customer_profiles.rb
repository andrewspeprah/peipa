class CreateCustomerProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_profiles do |t|
    	t.integer  :customer_id
    	t.integer  :profile_id
      t.timestamps
    end

    add_index :customer_profiles, [:id,:customer_id]
  end
end
