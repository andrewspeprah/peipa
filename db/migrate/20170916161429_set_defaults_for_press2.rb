class SetDefaultsForPress2 < ActiveRecord::Migration[5.1]
  def change
  	change_column :presses, :location_address, :text, default: ""
  	change_column :presses, :telephone, :string, default: ""
  	change_column :presses, :country_id,  :integer, default: 0
  end
end
