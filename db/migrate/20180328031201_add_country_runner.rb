class AddCountryRunner < ActiveRecord::Migration[5.1]
  def change
    add_column :runners, :country_id, :integer
  end
end
