class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.string   :name
      t.string   :description
      t.column   :privileges, :json
      t.timestamps
    end

    add_column :users, :role_id, :integer
    add_column :users, :role_name, :string, default: "staff"
  end
end
