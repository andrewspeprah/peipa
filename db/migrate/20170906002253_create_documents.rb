class CreateDocuments < ActiveRecord::Migration[5.1]
  def change
    create_table :documents do |t|
    	t.text		 :file_url
    	t.string 	 :filename
    	t.boolean  :is_archived, default: false
    	t.integer  :customer_id
      t.timestamps
    end

    add_index :documents, [:id, :customer_id]
  end
end
