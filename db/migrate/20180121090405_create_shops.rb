class CreateShops < ActiveRecord::Migration[5.1]
  def change
    create_table :shops do |t|
      t.string   :name
      t.text     :address
      t.string   :tag_line
      t.string   :telephone
      t.timestamps
    end
  end
end
