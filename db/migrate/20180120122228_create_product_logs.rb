class CreateProductLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :product_logs do |t|
      t.string  :msisdn
      t.string  :country
      t.integer :product_id
      t.text    :remarks
      t.text    :raw_log
      t.timestamps
    end
  end
end
