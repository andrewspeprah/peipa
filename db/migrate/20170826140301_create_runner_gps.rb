class CreateRunnerGps < ActiveRecord::Migration[5.1]
  def change
    create_table :runner_gps do |t|
    	t.integer	 :runner_id
    	t.integer	 :gp_id
      t.timestamps
    end

    add_index :runner_gps, [:id, :runner_id, :gp_id]
  end
end
