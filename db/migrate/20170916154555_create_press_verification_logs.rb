class CreatePressVerificationLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :press_verification_logs do |t|
    	t.integer 	:press_id
    	t.integer		:verification_log_id
      t.timestamps
    end

    remove_column :verification_logs, :customer_id
    remove_column :verification_logs, :press_id
  end
end
