class CreateCustomerGps < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_gps do |t|
    	t.integer	 :customer_id
    	t.integer	 :gp_id
      t.timestamps
    end

    add_index :customer_gps, [:id,:customer_id,:gp_id]
  end
end
