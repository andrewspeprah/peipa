class CreateVerificationLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :verification_logs do |t|
    	t.integer		:customer_id
    	t.string		:code
    	t.boolean		:is_verified, default: false
      t.timestamps
    end
  end
end
