class AddCurrencyToAccount < ActiveRecord::Migration[5.1]
  def change
  	add_column :accounts, :currency, :string, default: "GHS"
  	# add_column :customers, :password, :string
  	add_column :customers, :password_digest, :string
  end
end
