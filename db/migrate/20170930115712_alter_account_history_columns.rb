class AlterAccountHistoryColumns < ActiveRecord::Migration[5.1]
  def change
  	rename_column :account_histories, :new_balance, :new_balance_in_pesewas
  	change_column :account_histories, :new_balance_in_pesewas, :integer, default: 0
  	add_column :account_histories, :currency, :string, default: "GHS"
  end
end
