class ChangeNewBalanceColumn < ActiveRecord::Migration[5.1]
  def change
  	if column_exists?(:account_histories, :new_balance)
  		rename_column :account_histories, :new_balance, :new_balance_in_pesewas
  	end
  end
end
