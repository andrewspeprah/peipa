class AddFcmIdToDevice < ActiveRecord::Migration[5.1]
  def change
  	add_column :devices, :fcm_id, :string, default: " "
  end
end
