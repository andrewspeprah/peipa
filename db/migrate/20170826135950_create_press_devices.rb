class CreatePressDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :press_devices do |t|
    	t.integer		:press_id
    	t.integer		:device_id
      t.timestamps
    end
  end
end
