class AddIsApprovedPress < ActiveRecord::Migration[5.1]
  def change
  	add_column :presses, :is_approved, :boolean, default: false
  end
end
