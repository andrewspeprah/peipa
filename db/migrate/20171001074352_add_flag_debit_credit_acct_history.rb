class AddFlagDebitCreditAcctHistory < ActiveRecord::Migration[5.1]
  def change
  	add_column :account_histories, :transaction_type, :string, default: ""

  	AccountHistory.all.each do |acct_history|
  		case acct_history.service
  		when "order_confirmation"
  			acct_history.update_attribute(:transaction_type, "Debit")
  		when "voucher_topup"
  			acct_history.update_attribute(:transaction_type, "Credit")
  		end
  	end
  end
end
