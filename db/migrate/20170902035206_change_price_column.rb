class ChangePriceColumn < ActiveRecord::Migration[5.1]
  def change
  	remove_column :papers, :price
  	add_column :papers, :price_in_pesewas, :integer, default: 0
  end
end
