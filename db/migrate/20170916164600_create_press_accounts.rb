class CreatePressAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :press_accounts do |t|
    	t.integer 	:press_id
    	t.integer		:account_id
      t.timestamps
    end
  end
end
