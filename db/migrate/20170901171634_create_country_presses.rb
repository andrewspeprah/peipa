class CreateCountryPresses < ActiveRecord::Migration[5.1]
  def change
    create_table :country_presses do |t|
    	t.integer		:country_id
    	t.integer		:press_id
      t.timestamps
    end

    add_index :country_presses, [:id,:country_id]
  end
end
