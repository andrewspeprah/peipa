class CreateDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :devices do |t|
    	t.string	:uid
    	t.boolean	:is_active, default: true
      t.timestamps
    end
  end
end
