class CreateCountries < ActiveRecord::Migration[5.1]
  def change
    create_table :countries do |t|
    	t.string	:name
    	t.boolean	:is_active, default: true
      t.timestamps
    end

    add_index :countries, [:id]
  end
end
