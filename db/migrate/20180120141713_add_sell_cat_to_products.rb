class AddSellCatToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :category, :string
    add_column :products, :sell_price, :string
    rename_column :products, :price, :unit_price
  end
end
