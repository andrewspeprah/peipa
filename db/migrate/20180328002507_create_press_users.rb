class CreatePressUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :press_users do |t|
      t.integer   :press_id
      t.integer   :user_id
      t.timestamps
    end
  end
end
