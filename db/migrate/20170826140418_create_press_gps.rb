class CreatePressGps < ActiveRecord::Migration[5.1]
  def change
    create_table :press_gps do |t|
    	t.integer 	:press_id
    	t.integer		:gp_id
      t.timestamps
    end

    add_index :press_gps, [:id, :press_id, :gp_id]
  end
end
