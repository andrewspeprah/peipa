class RemoveCountryColumn < ActiveRecord::Migration[5.1]
  def change
  	remove_column :customers, :country, :string
  	add_column :customers, :country_id, :integer
  end
end
