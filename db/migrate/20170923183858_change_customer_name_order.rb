class ChangeCustomerNameOrder < ActiveRecord::Migration[5.1]
  def up
  	change_column :orders,:customer_name, :string, default: " "
  end

  def down
  	change_column :orders, :customer_name, :string, default: " "
  end
end
