class CreateRunnerAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :runner_accounts do |t|
    	t.integer		:runner_id
    	t.integer		:account_id
      t.timestamps
    end
  end
end
