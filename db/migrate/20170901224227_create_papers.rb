class CreatePapers < ActiveRecord::Migration[5.1]
  def change
    create_table :papers do |t|
    	t.string 	:size
    	t.string	:type
    	t.string	:color
    	t.string	:price
    	t.string 	:currency
    	t.string	:unit
      t.integer :service_id
      t.timestamps
    end

    add_index :papers, [:id, :service_id]
  end
end
