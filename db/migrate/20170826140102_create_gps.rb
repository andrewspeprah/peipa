class CreateGps < ActiveRecord::Migration[5.1]
  def change
    create_table :gps do |t|
    	t.string	:lat
    	t.string	:lng
    	t.string	:name
    	t.string	:country
    	t.string	:city
      t.timestamps
    end

    add_index :gps, [:id]
  end
end
