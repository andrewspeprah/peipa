class CreateOrderDocuments < ActiveRecord::Migration[5.1]
  def change
    create_table :order_documents do |t|
    	t.integer		:order_id
    	t.integer		:document_id
      t.timestamps
    end

    add_index :order_documents, [:id,:order_id]
  end
end
