class CreatePresses < ActiveRecord::Migration[5.1]
  def change
    create_table :presses do |t|
    	t.string		:name
    	t.text			:info
    	t.string		:location_address
    	t.string		:telephone
    	t.boolean		:is_active, default: true
    	t.boolean		:is_verified, default: false
      t.timestamps
    end
  end
end
