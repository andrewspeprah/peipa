class AddNoOfSheets < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :no_of_sheets, :integer, default: 0
  end
end
