class CreatePressRunners < ActiveRecord::Migration[5.1]
  def change
    create_table :press_runners do |t|
      t.integer   :press_id
      t.integer   :runner_id
      t.timestamps
    end
  end
end
