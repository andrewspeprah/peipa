class SetDefaultsForPress < ActiveRecord::Migration[5.1]
  def up
  	change_column :presses, :name, :string, default: ""
  	change_column :presses, :info, :string, default: ""
  end
end
