class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
    	t.integer	 	:amount_in_pesewas, default: 0
    	t.string 		:currency, default: "GHS"
    	t.string		:state, default: "pending_confirmation"
    	t.integer 	:press_id
    	t.integer		:customer_id
    	t.string		:press_name 			# In-case we loose press' name we still have their last name
    	t.integer		:customer_name		# In-case we loose customer, we still have their last name
      t.string    :uid
      t.column    :specs, :json
      t.text      :additional_info, default: ""
      t.timestamps
    end

    add_index :orders, [:id, :customer_id]
  end
end
