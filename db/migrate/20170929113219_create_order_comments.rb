class CreateOrderComments < ActiveRecord::Migration[5.1]
  def change
    create_table :order_comments do |t|
    	t.integer 	:order_id
    	t.integer		:comment_id
      t.timestamps
    end

    add_index :order_comments, [:id]
  end
end
