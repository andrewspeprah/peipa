class AddDefaultsPress < ActiveRecord::Migration[5.1]
  def change
  	change_column :presses, :name, :string, default: "--"
  	change_column :presses, :info, :string, default: "--"
  	change_column :presses, :location_address, :string, default: "--"
  	change_column :presses, :telephone, :string, default: "--"
  end
end

