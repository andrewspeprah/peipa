class AddIsUsedToVoucher < ActiveRecord::Migration[5.1]
  def change
  	add_column :vouchers, :is_printed, :boolean, default: false
  end
end
