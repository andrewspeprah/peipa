class CreateCustomerVerificationLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_verification_logs do |t|
    	t.integer	 :customer_id
    	t.integer  :verification_log_id
      t.timestamps
    end
  end
end
