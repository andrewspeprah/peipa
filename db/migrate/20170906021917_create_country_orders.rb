class CreateCountryOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :country_orders do |t|
    	t.integer 	:country_id
    	t.integer	  :order_id
      t.timestamps
    end

    add_index :country_orders, [:id,:country_id,:order_id]
  end
end
