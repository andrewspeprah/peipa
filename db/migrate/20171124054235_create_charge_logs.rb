class CreateChargeLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :charge_logs do |t|
    	t.integer 	:order_id
    	t.integer		:gross_amount_in_pesewas
    	t.integer		:charged_ammount_in_pesewas
      t.timestamps
    end

    add_index :charge_logs, [:id, :order_id]
  end
end
