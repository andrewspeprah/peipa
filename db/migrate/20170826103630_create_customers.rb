class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
    	t.string	:full_name
    	t.string	:msisdn
    	t.string	:country
    	t.text		:location_address
    	t.boolean	:is_active, default: true
      t.timestamps
    end
  end
end
