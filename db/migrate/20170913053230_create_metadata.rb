class CreateMetadata < ActiveRecord::Migration[5.1]
  def change
    create_table :metadata do |t|
    	t.integer	 :document_id
    	t.column 	 :params, :json, default: {}
      t.timestamps
    end
  end
end
