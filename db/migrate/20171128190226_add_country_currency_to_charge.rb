class AddCountryCurrencyToCharge < ActiveRecord::Migration[5.1]
  def change
  	add_column :charge_specs, :currency, :string, default: "GHS"
  	add_column :charge_specs, :country_id, :integer
  end
end
