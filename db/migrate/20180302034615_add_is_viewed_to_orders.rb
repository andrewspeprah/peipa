class AddIsViewedToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :is_viewed, :boolean, default: false
  end
end
