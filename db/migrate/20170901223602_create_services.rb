class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
    	t.string	:name
    	t.boolean	:is_active
    	t.string 	:description
    	t.column 	:params, :json
    	t.integer	:press_id
      t.timestamps
    end

    add_index :services, [:id,:press_id]
  end
end
