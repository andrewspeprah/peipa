class CreateCustomerAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :customer_accounts do |t|
    	t.integer 	:customer_id
    	t.integer		:account_id
      t.timestamps
    end
  end
end
