class CreateAccountHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :account_histories do |t|
    	t.integer		:amount_in_pesewas
    	t.string		:service
    	t.string		:remarks
    	t.integer		:transaction_id
    	t.integer		:account_id
    	t.bigint		:new_balance
      t.timestamps
    end

    add_index :account_histories, [:id]
  end
end
