class CreateSmsLogs < ActiveRecord::Migration[5.1]
  def change
  	#SmsLog.create(recipient: options[:msisdn],
    # message: options[:message],
    # service: options[:service],
    # status: status,
    # status_code: result.to_s)
    create_table :sms_logs do |t|
    	t.string	:recipient, default: ""
    	t.text		:message
    	t.string	:service
    	t.string	:status
    	t.string 	:status_code
      t.timestamps
    end
  end
end
