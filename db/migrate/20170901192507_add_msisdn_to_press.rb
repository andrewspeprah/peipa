class AddMsisdnToPress < ActiveRecord::Migration[5.1]
  def change
  	add_column :presses, :msisdn, :string
  	add_column :presses, :country_id, :integer
  	add_column :presses, :uid, :string
  end
end
