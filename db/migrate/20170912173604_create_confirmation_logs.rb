class CreateConfirmationLogs < ActiveRecord::Migration[5.1]
  def change
    create_table 	:confirmation_logs do |t|
    	t.integer		:customer_id
    	t.integer 	:press_id
    	t.integer		:amount_in_pesewas, default: 0 # Final amount customer agreed on
    	t.boolean 	:is_approved, default: false
      t.timestamps
    end
  end
end
