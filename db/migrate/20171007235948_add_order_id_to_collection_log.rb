class AddOrderIdToCollectionLog < ActiveRecord::Migration[5.1]
  def change
  	add_column :collection_logs, :order_id, :integer
  	add_column :collection_logs, :request_type, :string, default: "customer to press"
  end
end
