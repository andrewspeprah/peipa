class CreateCompanyProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :company_profiles do |t|
    	t.integer	 :company_id
    	t.integer  :profile_id
      t.timestamps
    end

    add_index :company_profiles, [:id, :company_id]
  end
end
