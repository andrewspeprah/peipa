class AddResponseToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :response, :text
  end
end
