class AddMd5ToFile < ActiveRecord::Migration[5.1]
  def change
  	add_column :documents, :md5_hash, :text
  end
end
