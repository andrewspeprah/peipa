class AddIsInvoiceToOrder < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :is_invoice, :boolean, default: true
  end
end
