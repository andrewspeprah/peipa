class CreateChargeSpecs < ActiveRecord::Migration[5.1]
  def change
    create_table :charge_specs do |t|
    	t.integer	 :amount_in_pesewas
    	t.integer	 :fee_in_pesewas
      t.timestamps
    end

    add_index :charge_specs, [:id]
  end
end
