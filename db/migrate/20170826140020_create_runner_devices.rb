class CreateRunnerDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :runner_devices do |t|
    	t.integer		:runner_id
    	t.integer		:device_id
      t.timestamps
    end
  end
end
