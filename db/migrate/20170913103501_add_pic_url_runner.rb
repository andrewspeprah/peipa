class AddPicUrlRunner < ActiveRecord::Migration[5.1]
  def change
  	add_column :runners, :pic_url, :string, default: ""
  	add_column :runners, :location_pic_url, :string, default: ""
  	add_column :deliveries, :order_id, :integer
  end
end
