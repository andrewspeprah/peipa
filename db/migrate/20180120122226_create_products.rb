class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string  :name
      t.string  :code
      t.string  :size
      t.string  :price
      t.string  :expiry_date
      t.string  :quantity
      t.timestamps
    end
  end
end
