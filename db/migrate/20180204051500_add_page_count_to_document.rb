class AddPageCountToDocument < ActiveRecord::Migration[5.1]
  def change
    add_column :documents, :page_count, :integer, default: 0
    add_column :documents, :is_calculated, :boolean, default: false
  end
end
