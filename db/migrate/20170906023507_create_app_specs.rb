class CreateAppSpecs < ActiveRecord::Migration[5.1]
  def change
    create_table :app_specs do |t|
    	t.integer  :client_id
    	t.column   :other_params, :json
      t.timestamps
    end

    add_index :app_specs, [:id,:client_id]
  end
end
