class AddPicUrlLocationUrlToPress < ActiveRecord::Migration[5.1]
  def change
  	add_column :presses, :pic_url, :string, default: ""
  	add_column :presses, :location_pic_url, :string, default: ""
  end
end
