class CreateDeliveries < ActiveRecord::Migration[5.1]
  def change
    create_table 	:deliveries do |t|
    	t.integer	 	:customer_id
    	t.integer		:runner_id
    	t.integer		:press_id
    	t.string		:status, default: "pending_collection"
    	t.boolean 	:is_delivered, default: false
    	t.integer		:amount_in_pesewas, default: 0 # Amount paid by customer for delivery
    	t.integer 	:commission_in_pesewas, default: 0 # Commission paid to runner for delivery document
      t.timestamps
    end

    add_index :deliveries, [:id, :customer_id]
  end
end
