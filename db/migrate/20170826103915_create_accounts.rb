class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
    	t.bigint 		:balance, default: 0
    	t.integer		:customer_id
    	t.boolean		:is_active, default: true
      t.timestamps
    end

    # add index to accounts
    add_index :accounts, [:id,:customer_id]

    # add customer uid to identify customers
    # as well as email for notifications
    add_column :customers, :email, :string
    add_column :customers, :uid, :string
  end
end
