class AddVerificationToCustomero < ActiveRecord::Migration[5.1]
  def change
  	add_column :customers, :is_verified, :boolean, default: false
  end
end
