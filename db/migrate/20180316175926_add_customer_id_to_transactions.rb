class AddCustomerIdToTransactions < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :customer_id, :integer
  end
end
