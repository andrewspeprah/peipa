class RenameTypeColumn < ActiveRecord::Migration[5.1]
  def change
  	rename_column :papers, :type, :ptype
  end
end
