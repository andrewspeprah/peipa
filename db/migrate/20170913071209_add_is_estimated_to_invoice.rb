class AddIsEstimatedToInvoice < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :is_estimated, :boolean, default: false
  end
end
