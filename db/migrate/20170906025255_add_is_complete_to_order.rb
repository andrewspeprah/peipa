class AddIsCompleteToOrder < ActiveRecord::Migration[5.1]
  def change
  	add_column :orders, :is_complete, :boolean, default: false
  end
end
