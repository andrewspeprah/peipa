class CreateRunners < ActiveRecord::Migration[5.1]
  def change
    create_table :runners do |t|
    	t.string	:name
    	t.string	:email
    	t.text		:location_address
    	t.string	:msisdn
    	t.boolean	:is_active, default: true
    	t.boolean	:is_verified, default: false
      t.timestamps
    end
  end
end
