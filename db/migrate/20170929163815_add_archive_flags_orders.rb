class AddArchiveFlagsOrders < ActiveRecord::Migration[5.1]
  def change
  	add_column 	:orders, :is_archived, :boolean, default: false
  	add_column 	:orders, :archive_flags, :string, default: "0,0,0"
  end
end
