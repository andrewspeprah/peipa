class AddPinSetToCustomers < ActiveRecord::Migration[5.1]
  def change
  	add_column :customers, :pin_set, :boolean, default: false
  end
end
