class ChangeDefaultValueFullName < ActiveRecord::Migration[5.1]
  def change
  	change_column :customers, :full_name, :string, default: " "
  end
end
