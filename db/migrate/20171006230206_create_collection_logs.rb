class CreateCollectionLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :collection_logs do |t|
    	t.string	:categoroy, default: "press_customer"
    	t.string 	:remarks, default: ""
      t.timestamps
    end
  end
end
