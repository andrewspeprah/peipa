class AddFromToComment < ActiveRecord::Migration[5.1]
  def change
  	add_column :comments, :from, :string, default: ""
  end
end
