class AddParamsToAccountHistory < ActiveRecord::Migration[5.1]
  def change
    add_column :account_histories, :params, :json, default: {}
  end
end
