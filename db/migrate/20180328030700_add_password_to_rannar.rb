class AddPasswordToRannar < ActiveRecord::Migration[5.1]
  def change
    add_column :runners, :password_digest, :string
  end
end
