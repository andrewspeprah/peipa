class ConvertBalanceColumn < ActiveRecord::Migration[5.1]
  def change
  	rename_column :accounts, :balance, :balance_in_pesewas
  end
end
