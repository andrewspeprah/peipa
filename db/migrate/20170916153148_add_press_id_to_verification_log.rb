class AddPressIdToVerificationLog < ActiveRecord::Migration[5.1]
  def change
  	add_column :verification_logs, :press_id, :integer
  end
end
