class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
    	t.string 	 :pic_url, default: ""
    	t.string 	 :location_pic_url, default: ""
    	t.text   	 :details, default: ""
    	t.column 	 :params, :json
      t.timestamps
    end
  end
end
