class CreateOrderExtracharges < ActiveRecord::Migration[5.1]
  def change
    create_table :order_extracharges do |t|
    	t.integer		:order_id
    	t.integer		:extra_charge_id
      t.timestamps
    end

    add_index :order_extracharges, [:id]
  end
end
