class CreateVouchers < ActiveRecord::Migration[5.1]
  def change
    create_table :vouchers do |t|
    	t.text  		:number, default: ""
    	t.datetime 	:expires_at
    	t.integer		:amount_in_pesewas, default: 0
      t.string    :currency, default: "GHS"
    	t.boolean		:is_used, default: false
      t.integer   :country_id
      t.timestamps
    end

    add_index :vouchers, [:id, :country_id]
  end
end
