class CustomersController < UsersController
	# Shows the list of customers registered
	# to a certain country
	# Default country: Ghana
	def index
		@customers = Customer.where("full_name LIKE ? OR msisdn LIKE ? OR uid LIKE ?","\%#{params[:q]}\%","\%#{params[:q]}\%","\%#{params[:q]}\%").page params[:page]
	end

	def show
		@customer = Customer.find(params[:id])
		unless @customer.present?
			flash[:error] = "Customer not found"
			redirect_to customers_url
			return ;
		end
	end

	private
	def build_query
		
	end
end
