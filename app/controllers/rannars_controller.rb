class RannarsController < UsersController

  # Get list of rannars
  def index
    @rannars = current_user.press.runners.page(params[:page] || 1)
  end

  # Shows form for registering new rannar
  def new
    @rannar = current_user.press.runners.new
  end

  # Saves a newly created rannar
  def create
    @rannar = current_user.press.runners.new(rannar_params)
    @rannar.country = current_user.press.country

    if @rannar.save
      current_user.press.runners << @rannar
      flash[:notice] = "Rannar successfully saved"
      redirect_to rannars_url
    else
      flash[:alert] = "Error saving rannar. Please try again. #{@rannar.errors.as_json}"
      render "new"
    end
  end

  def edit
  end

  def update
  end

  # Show available orders and
  # location delivery has to be sent to
  def delivery
    @deliveries = current_user.press.deliveries.page(params[:page] || 1)
  end

  # Shows view for assigning delivery
  def assign
    
  end

  private

  def rannar_params
    params.require(:runner).permit(:name, :email, :location_address, :msisdn, :password)
  end
end
