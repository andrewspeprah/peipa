class ServicesController < UsersController
  before_action :build_query
  # Service index page
  # Display list of available services
  def index
    @services = Service.where(@query)
      .page(params[:page] || 1)
      .order("created_at DESC")
  end

  # shows form for creating new service
  def new
    if current_user.has_press?
      @service = current_user.press.services.new
    else
      @service = Service.new
    end
  end

  # Saves a new service
  def create
    @service = current_user.press.services.new(service_params)
		if @service.save
			flash[:notice] = "Service successfully saved"
			redirect_to services_url
		else
			flash[:alert] = "Error saving service. Please try again. #{@service.errors.as_json}"
			render "new"
		end
  end

  # Shows a particular service
  def show
    find_service
  end

  # Provides a form for editing service
  def edit
    find_service
  end

  # Updates a service
  def update
    find_service

		if @service.update_attributes(service_params)
			flash[:notice] = "Service has successfully been updated."
			redirect_to services_url
		else
			flash[:alert] = "An error was encountered while updating service. Please try again later"
			render "edit"
		end
  end

  def destroy
    find_service
    @service.destroy
    flash[:notice] = "Service successfully removed."
    redirect_to services_url
  end

  private

  def build_query
    @query = []

    # If the user belongs to a press
    # scope the displayed inormation
    if current_user.has_press?
      @query << "press_id = '#{current_user.press.id}'"
    end

    if params[:q].present?
      @query << "name LIKE '\%#{params[:q]}\%'"
    end

    # Add the query strings together
    @query = @query.join(" AND ")
  end

  def service_params
		params.require(:service).permit(:name, :description)
  end
  
  def find_service
    @service = current_user.press.services.find(params[:id])

		unless @service.present?
			flash[:alert] = "Error.. Service not found."
			redirect_to services_url
			return ;
		end
  end
end
