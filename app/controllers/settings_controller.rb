class SettingsController < UsersController

	# Index page of settings
	def index
		@user = current_user
	end

	# Update user profile settings
	def update
		@user = current_user
		if current_user.update_attributes(user_params)
			flash[:notice] = "Profile update successful."
		else
			flash[:alert] = "Profile update failed"
		end

		redirect_to settings_url
	end

	# changes password
	def password
		if current_user.valid_password?(params[:current_password])
			if current_user.update_attribute(:password, params[:password])
				flash[:notice] = "Password successfully changed."
			else
				flash[:alert] = "Error changing password. Please try again.#{current_user.errors.as_json}"
			end
		else
			flash[:alert] = "Current password is invalid."
		end

		redirect_to settings_url
	end

	private
	def user_params
		{first_name: params[:first_name],
		last_name: params[:last_name],
		email: params[:email]}
	end
end
