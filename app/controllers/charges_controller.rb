class ChargesController < UsersController
	before_action :check_press # checks whether the user is in a press

	def index
		@charges = ChargeSpec.all
	end

	# Shows interface for adding new
	# charge specification
	def new
		@charge_spec = ChargeSpec.new
	end


	# Saves a new charge
	def create
		@charge_spec = ChargeSpec.new(charge_spec_params)
		@charge_spec.amount = params[:charge_spec][:amount].to_money
		@charge_spec.fee = params[:charge_spec][:fee].to_money
		if @charge_spec.save
			flash[:notice] = "Charge saved"
			redirect_to charges_url
		else
			flash[:alert] = "Error saving charge. Please try again. #{@charge_spec.errors.as_json}"
			render "new"
		end
	end

	# To edit an existing charge
	def edit
		@charge_spec = ChargeSpec.find(params[:id])

		unless @charge_spec.present?
			flash[:alert] = "Error.. Charge not found."
			redirect_to charges_url
			return ;
		end
	end

	# Updates an existing charge
	def update
		@charge_spec = ChargeSpec.find(params[:id])

		unless @charge_spec.present?
			flash[:alert] = "Error.. Charge not found."
			redirect_to charges_url
			return ;
		end

		@charge_spec.amount = params[:charge_spec][:amount].to_money
		@charge_spec.fee = params[:charge_spec][:fee].to_money
		if @charge_spec.update_attributes(charge_spec_params)
			flash[:notice] = "Charge has successfully been updated."
			redirect_to charges_url
		else
			flash[:alert] = "An error was encountered while updating charge. Please try again later"
			render "edit"
		end
	end

	# Removes an existing charge
	def delete
		charge_spec = ChargeSpec.find(params[:id])
	end

	private
	def charge_spec_params
		params.require(:charge_spec).permit(:amount, :fee, :country_id)
	end
	
	def check_press
		if current_user.has_press?
			redirect_to root_url
			return ;
		end
	end
end
