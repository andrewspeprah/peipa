class CountriesController < UsersController
	before_action :check_press

	# Displays all countries
	def index
		query = gen_filter_query
		@countries = Country.where(query).page params[:page]
	end

	# Shows a single press
	def show
		@country = Country.find_by_id(params[:id])
	end

end
