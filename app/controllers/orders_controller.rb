class OrdersController < UsersController
  before_action :build_query, only: [:index]
  before_action :load_order, only: [:download, :show, :status]

  # Provide list of orders
  # according to search filter provided
  def index
    @orders = Order.where(@query)
      .order("created_at DESC")
      .page(params[:page] || 1)
  end

  # Shows the details of a transaction
  # Allow support team to help with transaction
  # issues
  def show
  end

  # Downloads the files of the order
  # onto the machine
  def download
    bucket = $amazon_s3.bucket("peipadbapp")

    files = @order.documents.pluck(:file_url)

    # Open the zip stream
    require "zip"
    zip_stream = Zip::OutputStream.write_buffer do |zip|

      # Loop through the files we want to zip
      files.each do |file_name|

        # Get the file object
        file_obj = bucket.object("#{file_name}")

        # Give a name to the file and start a new entry
        zip.put_next_entry(file_name.split("/").last)

        # Write the file data to zip
        begin
          zip.print file_obj.get.body.read
        rescue => exception
          flash[:error] = "Please check your internet connection."
          redirect_to orders_path(view_status: params[:view_status])
          return
        end
      end
    end

    # Rewind the IO stream so we can read it
    zip_stream.rewind

    # Create a temp file for the zip
    tempZip = Tempfile.new([@order.uid, ".zip"])

    # Write the stringIO data
    tempZip.binmode
    tempZip.write zip_stream.read

    send_file tempZip
    # Send it to s3
    # zip_file.upload_file(tempZip)
    # Clean up the tmp file
    # tempZip.close
    # tempZip.unlink
  end

  # Change the status of an order
  def status
    customer = @order.customer
    params[:country] = customer.country.name
    params[:msisdn] = @order.press.msisdn
    params[:order_uid] = @order.uid

    # Process the order and get the status
    result = PeipaPress::OrderLib::Engine.process(service: "status",
                                                  params: params)

    if result[:success]
      flash[:notice] = result[:message]
    else
      flash[:error] = result[:message]
    end

    redirect_to orders_path(view_status: params[:view_status])
  end

  def load_order
    @order = Order.find(params[:id])
    unless @order.present?
      flash[:error] = "Order not found"
      redirect_to orders_path(view_status: params[:view_status])
      return
    end
  end

  private

  def build_query
    @query = []
    if params[:q].present?
      @query += ["uid LIKE '\%#{params[:q].upcase}\%'"]
    end

    if params[:from].present?
      @query += ["created_at >= '#{params[:from].strip.to_time}'"]
    end

    if params[:to].present?
      @query += ["created_at <= '#{params[:to].strip.to_time}'"]
    end

    if params[:cid].present?
      @query += ["customer_id = '#{params[:cid]}'"]
    end

    if current_user.press.present?
      @query += ["press_id = '#{current_user.press.id}'"]
    end

    # Viewing status of the order
    if params[:v_stat].present?
      case params[:v_stat]
      when "new"
        @query += ["is_viewed = false"]
      when "viewed"
        @query += ["is_viewed = true"]
      else
      end
    end

    # Pull all unarchived transactions
    @query += ["is_archived = false"]

    @query = @query.join(" AND ")
  end
end
