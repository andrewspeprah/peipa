class TransactionsController < UsersController

	# Index page
	def index
		query = gen_filter_query
		@transactions = AccountHistory.where(query).order("created_at DESC").page params[:page]
	end

	# Export link
	def export_csv
		query = gen_filter_query
		transactions = AccountHistory.where(query).order("created_at DESC")
		csv_file = AccountHistory.generate_csv(transactions)
		send_data csv_file, filename: "#{@filename}_transactions-#{Date.today}.csv"
	end

	private
	# Generate filter query
	def gen_filter_query
		query_list = []
		@filename = []

		# Check date range
		if params[:from].present?
			query_list << "created_at >= '#{params[:from].strip.to_time}'"
			@filename << params[:from]
		end

		if params[:to].present?
			query_list << "created_at <= '#{params[:to].strip.to_time}'"
			@filename << params[:to]
		end

		# Check status
		if params[:status]
			unless params[:status] == "all"
				query_list << "status = '#{params[:status].titleize}'"
			end
		end

		# set filter
		@filter = {from: params[:from],to: params[:to],
								msisdn: params[:msisdn],
								provider_reference: params[:provider_reference],
								status: params[:status],
								provider: params[:provider]}

		# get file name
		@filename = @filename.join("_")

		# Build query
		return query_list.join(" AND ")
	end
end
