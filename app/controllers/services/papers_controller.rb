class Services::PapersController < ServicesController
  before_action :find_service

  def index
  end

  def new
    @paper = @service.papers.new
  end

  def create
    @paper = @service.papers.new(paper_params)
    @paper.price *= 100    
    if @paper.save
			flash[:notice] = "Paper successfully saved"
			redirect_to service_url(@service)
		else
			flash[:alert] = "Error saving paper. Please try again. #{@paper.errors.as_json}"
			render "new"
		end
  end

  def edit
    find_paper
  end

  def update
    find_paper
    paper_params[:price] = paper_params[:price] * 1000

    if @paper.update_attributes(paper_params)
      @paper.price *= 100
      @paper.save
			flash[:notice] = "Paper has successfully been updated."
			redirect_to service_url(@service)
		else
			flash[:alert] = "An error was encountered while updating paper. Please try again later"
			render "edit"
		end
  end

  # Removes a paper
  def destroy
    find_paper
    flash[:notice] = "Paper successfully removed"
    @paper.destroy
    redirect_to service_url(@service)
  end

  private 
  def find_service
    @service = current_user.press.services.find(params[:id])

    unless @service.present?
      redirect_to services_url
      return ;
    end
  end

  def find_paper
    @paper = @service.papers.find(params[:pid])
    unless @paper.present?
      redirect_to service_url(@service)
      return ;
    end
  end

  def paper_params
		params.require(:paper).permit(:size,:ptype,:unit,:price,:currency,:color)
	end
end
