class PressesController < UsersController
	before_action :build_query, only: [:index]
	before_action :check_press

	def index
		@presses = Press.where(@query).order("created_at DESC")
	end

	def show
		@press = Press.find(params[:id])
	end

	# Shows the inventory and 
	# items left
	def inventory
	end

	# Shows charts to how many
	# items are left
	def metrics
	end

	# Toggle the state of the press
	# based on parameter provided
	def toggle
		press = Press.find(params[:id])

		unless press.present?
			flash[:alert] = "Press not found"
			redirect_to :back
			return 
		end

		# If press is present
		# make changes
		attribute = params[:attribute]
		if press.update_attribute(:"is_#{attribute}",!(press.send("is_#{attribute}")))
			flash[:notice] = "Press' #{attribute} state successfully change"
		else
			flash[:alert] = "Error changing press state. Please try again later."
		end

		# Get message for notification
		message = get_message(params[:attribute], params[:value] == "yes")

		# Send notification to press
		#Resque.enqueue(FcmWorkers::NotifyWorker, user_type: "press",
		#										country_id: press.country.id,
		#										user_id: press.id,
		#										message: message)
		options = {user_type: "press",
			country_id: press.country.id,
			user_id: press.id,
			message: message}
		NotifyWorker.perform_async(1.second,options)
	
		redirect_to presses_path
	end

	# Create the right message for notification
	def get_message(attribute,positive = true)
		case attribute
		when "verified"
			positive ? "Your account has now been #{attribute}. Thanks for using Peipa." : "Sorry, your account has been unverified. Please contact Peipa support for more enquiries." 
		when "approved"
			positive ? "Your account has now been #{attribute}. Thanks for using Peipa." : "Sorry, your account has been unapproved. Please contact Peipa support for more enquiries." 
		when "active"
			positive ? "Your account has now been activated. Thanks for using Peipa." : "Sorry, your account has been disactivated. Please contact Peipa support for more enquiries." 
		end
	end

	private
	def build_query
		@query = ["uid LIKE ?","\%#{params[:q]}\%"]
	end
end
