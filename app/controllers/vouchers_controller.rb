class VouchersController < UsersController
	before_action :load_countries

	# Show already generated vouchers
	def index
		@voucher_groups = Voucher.group(:amount_in_pesewas,:is_used,:currency).count(:amount_in_pesewas)
	end

	def show
		@voucher_groups = Voucher.group(:amount_in_pesewas,:is_used,:currency).count(:amount_in_pesewas)
	end

	# Shows the new interface for creating
	# new vouchers
	def new
	end

	# Creates new vouchers
	def create
		# TODO: Check password
		unless current_user.authenticate(params[:password])
			flash[:alert] = "Wrong password. Please try again"
			redirect_to vouchers_url
			return ;
		end

		# Generate vouchers
		params[:quantity].to_i.times do |i|
			Peipa::VoucherLib::Engine.process(service: "generate", country: params[:country], amount: params[:value].to_f * 100)
		end

		flash[:notice] = "Vouchers created. "
		redirect_to vouchers_url
	end

	# Printing a voucher
	def print
	end

	# Generates pdf of vouchers
	def get_pdf
		# TODO: Check password
		unless current_user.authenticate(params[:password])
			flash[:alert] = "Wrong password. Please try again"
			redirect_to vouchers_url
			return ;
		end

		# Get specified vouchers
		country = Country.find_by_name(params[:country])
		unless country.present?
			flash[:alert] = "Country not present"
			redirect_to vouchers_url
			return ;
		end

		@vouchers = country.vouchers
											 .unexpired
											 .where(amount_in_pesewas: params[:value].to_f * 10000,
															is_used: false,
															is_printed: false)
											 .limit(params[:quantity].to_i)

		# Mark those vouchers as printed
		respond_to do |format|
      format.html
      format.pdf do
        render pdf: "vouchers",
        			 margin:  {   top:               5,                     # default 10 (mm)
                            bottom:            5,
                            left:              10,
                            right:             10 }
      end
    end
	end


	private
	def load_countries
		@countries = Country.available
		@voucher_values = ["0.50","1","2","5","10","20","50"]
	end
end
