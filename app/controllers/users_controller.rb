class UsersController < ApplicationController
	before_action :authenticate_user!

	def index
	end

	# Generate filter query
	def gen_filter_query
		query_list = []
		@filename = []

		# Check date range
		if params[:from].present?
			query_list << "created_at >= '#{params[:from].strip.to_time}'"
			@filename << params[:from]
		end

		if params[:to].present?
			query_list << "created_at <= '#{params[:to].strip.to_time}'"
			@filename << params[:to]
		end

		# Check customer telephone
		if params[:msisdn].present?
			query_list << "msisdn LIKE '%#{params[:msisdn].strip}%'"
		end

		# Check provider reference
		if params[:provider_reference].present?
			query_list << "provider_reference LIKE '%#{params[:provider_reference].strip}%'"
		end

		# Check status
		if params[:status]
			unless params[:status] == "all"
				query_list << "status = '#{params[:status].titleize}'"
			end
		end

		# Check provider
		if params[:provider]
			unless params[:provider] == "all"
				query_list << "provider = '#{params[:provider]}'"
			end
			@filename << params[:provider]
		else
			@filename << "all"
		end

		# set filter
		@filter = {from: params[:from],to: params[:to],
								msisdn: params[:msisdn],
								provider_reference: params[:provider_reference],
								status: params[:status],
								provider: params[:provider]}

		# get file name
		@filename = @filename.join("_")

		# Build query
		return query_list.join(" AND ")
	end

	def check_press
		unless current_user.has_press?
			redirect_to root_url
			return ;
		end
	end
end
