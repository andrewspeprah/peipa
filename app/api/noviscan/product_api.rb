module Noviscan
    class ProductAPI < Grape::API
      helpers do
        private
      end

        # For posting a withdrawal
        resource :products do
            desc 'Saves an existing product / Creates a non existing product'

            params do
                optional :msisdn,   type: String, desc: 'Telephone number of customer.'
                optional :country,  type: String, desc: 'Country of customer.'
                optional :code,     type: String, desc: 'product unique code'
                requires :name,     type: String, desc: 'Name of product'
                optional :size,     type: String, desc: 'Product Size'
                optional :sell_price,    type: String, desc: "Product's price"
                optional :unit_price,    type: String, desc: "Product's price"
                optional :expiry_date, type: String, desc: "Expiry date"
                optional :quantity, type: String, desc: "Product quantity"
            end

            post do
                if params[:code].present?
                    product = Product.find_by_code(params[:code])
                elsif params[:name].present?
                    product = Product.find_by_name(params[:name])
                end

                # If product is present
                # update the product and
                # save it in the logs
                if product.present?
                    if product.update_attributes(name: params[:name], 
                                                code: params[:code],
                                                size: params[:size],
                                                unit_price: params[:unit_cost],
                                                sell_price: params[:sell_price],
                                                category: params[:category],
                                                expiry_date: params[:expiry_date],
                                                quantity: params[:quantity],
                                                picture_url: params[:picture_url])
                        # Update product logs
                        ProductLog.create(msisdn: params[:msisdn],
                                          country: params[:country],
                                          product_id: product.id,
                                          remarks: "Product update by #{params[:msisdn]} #{params[:username]}",
                                          raw_log: "#{params.to_json}")
                        return {success: true, message: "Product information successfully updated."}
                    else
                        return {success: false, message: "An error occured while updating the product details."}
                    end
                else
                    # Save new product
                    product = Product.new(name: params[:name], 
                                        code: params[:code],
                                        size: params[:size],
                                        unit_price: params[:unit_cost],
                                        sell_price: params[:sell_price],
                                        category: params[:category],
                                        expiry_date: params[:expiry_date],
                                        quantity: params[:quantity],
                                        picture_url: params[:picture_url])
                    if product.save
                        # Update product logs
                        ProductLog.create(  msisdn: params[:msisdn],
                                            country: params[:country],
                                            product_id: product.id,
                                            remarks: "New product created by #{params[:msisdn]} #{params[:username]}",
                                            raw_log: "#{params.to_json}")
                        return {success: true, message: "New product successfully saved"}
                    else
                        return {success: false, message: "Error while saving new product"}
                    end
                end
            end

            desc 'Get existing product'

            params do
                optional :msisdn,   type: String, desc: 'Telephone number of customer.'
                optional :country,  type: String, desc: 'Country of customer.'
                optional :code,     type: String, desc: 'product unique code'
                optional :name,     type: String, desc: 'Name of product'
                optional :id,       type: String, desc: "Id of product"
            end

            get do
                if params[:code].present?
                    product = Product.where("code LIKE ?","\%#{params[:code]}\%")
                elsif params[:name].present?
                    product = Product.where("name LIKE ?","\%#{params[:name]}\%").limit(10)
                else
                    product = Product.where("code LIKE ? OR name LIKE ? OR id = ?","\%#{params[:code]}\%","\%#{params[:name]}\%",params[:id]).limit(10)
                end

                if product.present?
                    return {success: true, message: "Product found", products: product.as_json}
                else
                    return {success: false, message: "No product found", products: product.as_json}
                end
            end
        end
    end
end
