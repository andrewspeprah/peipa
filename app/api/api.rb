class API < Grape::API
	prefix 'api'
  version 'v1', using: :path
  format :json

  # before do
  #   header 'API-Key', ENV['api_key_header']
  # end
  mount Peipa::Callback
  mount Peipa::FcmApi

  # --- Customer APIs
  mount Peipa::CustomerApi::AccountAPI
  mount Peipa::CustomerApi::CustomerAPI
  mount Peipa::CustomerApi::VoucherAPI
  mount Peipa::CustomerApi::PressAPI
  mount Peipa::CustomerApi::OrderAPI
  mount Peipa::CustomerApi::FilesAPI
  mount Peipa::CustomerApi::ServiceAPI
  mount Peipa::CustomerApi::GpAPI
  mount Peipa::CustomerApi::CloudAPI

  # --- Press APIs
  mount Peipa::PressApi::ProfileAPI   # Login / Signup / Verification / Authentication / Profile editing
  mount Peipa::PressApi::OrderAPI     # Orders sent to press
  mount Peipa::PressApi::SettingsAPI  # Setting Paper types and services rendered
  mount Peipa::PressApi::AccountAPI   # Balance / PaymentRequest / AccountHistory
  mount Peipa::PressApi::GpAPI        # Press Location

  # --- Runner APIs
  mount Peipa::RunnerApi::ProfileAPI  # Login / Signup / Verification / Authentication / Profile editing
  mount Peipa::RunnerApi::OrderAPI    # Order List / Delivered Orders / Current Order and customer profile
  mount Peipa::RunnerApi::AccountAPI  # Balance / PaymentRequest / AccountHistory
  mount Peipa::RunnerApi::GpAPI       # Runner's gps location

  # --- Noviscan API
  # TODO: Remove to separate project
  mount Noviscan::ProductAPI
end