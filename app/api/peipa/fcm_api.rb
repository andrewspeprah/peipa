module Peipa
  class FcmApi < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// api.floxchange.com/api/v1/callback.json
    namespace :customer do
      resource :fcm do
        desc "Updates the fcm"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :fcm_id,   type: String, desc: "FCM ID of the device"
          requires :user_type, type: String, desc: "User types | customer | press | runner"
        end

        post do
          return Peipa::FcmLib::Engine.process(service: "update_fcm",
                                                 params: params)
        end
      end
    end

    namespace :presses do
      resource :fcm do
        desc "Updates the fcm"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :fcm_id,   type: String, desc: "FCM ID of the device"
          requires :user_type, type: String, desc: "User types | customer | press | runner"
        end

        post do
          return Peipa::FcmLib::Engine.process(service: "update_fcm",
                                                 params: params)
        end
      end
    end

    namespace :runners do
      resource :fcm do
        desc "Updates the fcm"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :fcm_id,   type: String, desc: "FCM ID of the device"
          requires :user_type, type: String, desc: "User types | customer | press | runner"
        end

        post do
          return Peipa::FcmLib::Engine.process(service: "update_fcm",
                                                 params: params)
        end
      end
    end

  end
end
