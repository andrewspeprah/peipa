module Peipa::PressApi
  class AccountAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :presses do
      namespace :accounts do
        # For posting a withdrawal
        resource :withdrawal do
          desc 'Creates a new paper for service'

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            requires :category, type: String, desc: 'Category of withdrawal'
            requires :amount,   type: String, desc: 'Amount to be withdrawn'
            requires :details,  type: String, desc: 'Details of the transaction'
          end

          post do
            return PeipaPress::AccountLib::Engine.process(service: "withdrawal",
                                                          params: params)
          end
        end

        # Get the history of transactions
        resource :history do
          desc 'Creates a new user on Peipa.'

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
          end

          get do
            return PeipaPress::AccountLib::Engine.process(service: "history",
                                                      params: params)
          end
        end
      end # EOF - profile namespace
    end # EOF - runner namespace

  end
end
