module Peipa::PressApi
  class ProfileAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :presses do
      namespace :profile do
        resource :login do
          desc 'Creates a new paper for service'

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            requires :imei,     type: String, desc: 'IMEI'
          end

          post do
            return PeipaPress::ProfileLib::Engine.process(service: "login",
                                                          params: params)
          end
        end

        resource :verify do
          desc 'Verifies customer account'

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            requires :code,     type: String, desc: 'Verification code'
          end

          post do
            return PeipaPress::ProfileLib::Engine.process(service: "verify",
                                                          params: params)
          end
        end

        resource :update do
          desc 'Update cpress details'

          params do
            requires :msisdn,             type: String, desc: 'Telephone number of customer.'
            requires :country,            type: String, desc: 'Country of customer.'
            requires :name,               type: String, desc: 'Name of the printing press'
            requires :info,               type: String, desc: 'Information about the press'
            requires :location_address,   type: String, desc: 'Location address of press'
            requires :telephone,          type: String, desc: "Telephone of printing press"
            requires :location_pic,       type: String, desc: "Location pic url"
            requires :profile_pic,        type: String, desc: "Profile picture of press"
          end

          put do
            return PeipaPress::ProfileLib::Engine.process(service: "update_profile",
                                                          params: params)
          end
        end

        resource :details do
          desc 'Provides profile details'

          params do
            requires :msisdn,             type: String, desc: 'Telephone number of customer.'
            requires :country,            type: String, desc: 'Country of customer.'
          end

          get do
            return PeipaPress::ProfileLib::Engine.process(service: "details",
                                                          params: params)
          end
        end

        # Logout printing press
        resource :logout do
          desc 'logout Press'

          params do
            requires :msisdn,             type: String, desc: 'Telephone number of customer.'
            requires :country,            type: String, desc: 'Country of customer.'
          end

          post do
            return PeipaPress::ProfileLib::Engine.process(service: "logout",
                                                          params: params)
          end
        end

        # Logout printing press
        resource :update_device do
          desc 'Updates device information'

          params do
            requires :msisdn,             type: String, desc: 'Telephone number of customer.'
            requires :country,            type: String, desc: 'Country of customer.'
            requires :fcm_id,             type: String, desc: 'Firebase Messenger ID.'
            requires :device_uid,         type: String, desc: 'Devices unique ID.'
          end

          post do
            return PeipaPress::ProfileLib::Engine.process(service: "device_update",
                                                          params: params)
          end
        end

      end # EOF - profile namespace
    end # EOF - runner namespace
  end
end
