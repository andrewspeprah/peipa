module Peipa::PressApi
  class OrderAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :presses do
      namespace :orders do
#------------------------------------------------------------------------------------------------
        # Gives the list of orders
        resource :list do      
          desc "Provides the list of orders"

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
          end

          get do
            return PeipaPress::OrderLib::Engine.process(service: "list",
                                                      params: params)
          end
        end
#------------------------------------------------------------------------------------------------
        # Provides the details of an order
        # sent to their phone
        resource :details do      
          desc "Pushes confirmation prompt to customer to approve"

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            requires :order_uid, type: String, desc: 'Order id'
          end

          get do
            return PeipaPress::OrderLib::Engine.process(service: "details",
                                                        params: params)
          end
        end
#------------------------------------------------------------------------------------------------
        # Posts a confirmation request to customer
        # sent to their phone
        resource :state do      
          desc "Pushes confirmation prompt to customer to approve"

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            requires :order_uid, type: String, desc: 'Order id'
            requires :state,    type: String, desc: "New state of order - reviewed | confirmed | rejected | completed | delivered "
            optional :comment,  type: String, desc: "Comment in relation to order"
          end

          post do
            return PeipaPress::OrderLib::Engine.process(service: "status",
                                                        params: params)
          end
        end
#------------------------------------------------------------------------------------------------      
        # Posts a confirmation request to customer
        # sent to their phone
        resource :comments do      
          desc "Retrieves comments on order"

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            requires :order_uid, type: String, desc: 'Order id'
          end

          get do
            return PeipaPress::OrderLib::Engine.process(service: "comments",
                                                        params: params)
          end
        end
#------------------------------------------------------------------------------------------------     
      end # EOF - orders namespace
    end # EOF - runner namespace
  end
end
