module Peipa::PressApi
  class SettingsAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :presses do
      namespace :settings do
        resource :create do
          desc 'Creates a new paper for service'

          params do
          end

          post do
          end
        end
      end # EOF - profile namespace
    end # EOF - runner namespace

  end
end
