module Peipa
  class Callback < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https://{BASE_URL}/api/v1/callback.json
    resource :callback do
      desc "Callback URL"

      get do
        #If request contains no body return this message
        return {success: true, message: "Hello.. Welcome to Peipa"}
      end

      post do
        # If response is not empty
        # save response in a telcos_response.log file
        TELCO_RESPONSES.info("#{params.to_json}")
        
        # # Check the ip address of the person posting the request
        # unless request.env['REMOTE_ADDR'] == ENV["callback_ip"]
        #   return {success: false, message: "Unauthorized access"}
        # end
        
        #If request contains no body return this message
        if params.empty?
          return {success: true, message: "Hello.. Welcome to Peipa"}
        end

        # TODO: Process callback responses
        # here

        return {success: true, message: "Callback received"}
      end
    end
  end
end
