module Peipa::CustomerApi
  class OrderAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :customer do
    namespace :orders do
      resource :create do
        desc 'Creates a new order for customer.'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :document_type,     type: String, desc: 'Document type'
          requires :paper_type,     type: String, desc: 'Paper type being used'
          optional :has_copies,     type: String, desc: 'Presence of photocopies'
          optional :copy_paper_type, type: String, desc: 'The paper type being used for copies'
          requires :no_of_copies,     type: String, desc: 'Number of copies'
          optional :additional_info,  type: String, desc: 'Additional Information'
          requires :service_type, type: String, desc: 'Service type being requested for.'
          requires :press_id, type: String, desc: "Id of press"
          optional :has_delivery, type: String, desc: "Determines whether there is a delivery request"
          optional :delivery_location, type: String, desc: "Provides the delivery location of an order"
        end

        post do
          return Peipa::OrderLib::Engine.process(service: "create",
                                                    params: params)
        end
      end
      
      resource :list do      
        desc "Provides the list of orders"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :user,     type: String, desc: 'Selects the user either Customer/Press'
        end

        get do
          return Peipa::OrderLib::Engine.process(service: "list",
                                                    params: params)
        end
      end

      resource :location do      
        desc "Provides the location for an order"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :order_uid, type: String, desc: 'Order id'
        end

        get do
          return Peipa::OrderLib::Engine.process(service: "location",
                                                    params: params)
        end
      end

      # Press posting confirmation prompt to customer
      # to approve transaction
      resource :press_confirm do      
        desc "Pushes confirmation prompt to customer to approve"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :order_id, type: String, desc: 'Order id'
          requires :extra_cost, type: String, desc: "Extra cost that would be inquired by customer"
          requires :usertype, type: String, desc: 'User who posted the request'
        end

        post do
            return Peipa::OrderLib::Engine.process(service: "press_confirmation",
                                                      params: params)
        end
      end


      # Customer replying to confirmation prompt
      # sent to their phone
      resource :customer_confirm do      
        desc "Pushes confirmation prompt to customer to approve"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :order_id, type: String, desc: 'Order id'
          requires :extra_cost, type: String, desc: 'Extra cost that would be accepted by customer'
          requires :state,    type: String, desc: "Confirmation state - approved / declined"
        end

        post do
          return Peipa::OrderLib::Engine.process(service: "customer_confirmation",
                                                      params: params)
        end
      end


      # Provides the details of an order
      # sent to their phone
      resource :details do      
        desc "Pushes confirmation prompt to customer to approve"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :order_uid, type: String, desc: 'Order id'
        end

        get do
          return Peipa::OrderLib::Engine.process(service: "details",
                                                      params: params)
        end
      end

#------------------------------------------------------------------------------------------------
        # Posts a confirmation request to customer
        # sent to their phone
        resource :state do      
          desc "Pushes confirmation prompt to customer to approve"

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            requires :order_uid, type: String, desc: 'Order id'
            requires :state,    type: String, desc: "New state of order - reviewed | confirmed | rejected | completed | delivered | collection"
            optional :comment,  type: String, desc: "Comment in relation to order"
          end

          post do
            return Peipa::OrderLib::Engine.process(service: "status",
                                                   params: params)
          end
        end
#------------------------------------------------------------------------------------------------      
        # Posts a confirmation request to customer
        # sent to their phone
        resource :comments do      
          desc "Pushes confirmation prompt to customer to approve"

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            requires :order_uid, type: String, desc: 'Order id'
          end

          get do
            return Peipa::OrderLib::Engine.process(service: "comments",
                                                   params: params)
          end
        end
#------------------------------------------------------------------------------------------------          
    end
    end
  end
end
