module Peipa::CustomerApi
  class VoucherAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :customer do
    namespace :vouchers do
      resource :topup do
        desc 'Creates a new user on Peipa.'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :voucher_code,     type: String, desc: 'Voucher number'
        end

        post do
          return Peipa::VoucherLib::Engine.process(service: "topup",
                                                    params: params)
        end
      end
    end
    end
  end
end
