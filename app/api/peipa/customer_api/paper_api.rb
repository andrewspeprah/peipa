module Peipa::CustomerApi
  class PaperAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    # {"id"=>nil, "size"=>nil, "ptype"=>nil, "color"=>nil, "currency"=>nil, "unit"=>nil, "service_id"=>nil, "created_at"=>nil, "updated_at"=>nil, "price_in_pesewas"=>0}
    namespace :customer do
    namespace :papers do
      resource :create do
        desc 'Creates a new paper for service'

        params do
          requires :msisdn,    type: String, desc: 'Telephone number of customer.'
          requires :country,   type: String, desc: 'Country of customer.'
          requires :size,      type: String, desc: 'Paper size'
          requires :ptype,     type: String, desc: 'Paper type'
          requires :color,     type: String, desc: 'Paper type'
        end

        post do
          return Peipa::PressLib::Engine.process(service: "service|registration",
                                                    params: params)
        end
      end
      
      resource :list do      
        desc "Provides the list of orders"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :user,     type: String, desc: 'Selects the user either Customer/Press'
        end

        get do
          return Peipa::OrderLib::Engine.process(service: "list",
                                                    params: params)
        end
      end
    end
    end
  end
end
