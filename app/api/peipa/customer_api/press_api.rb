module Peipa::CustomerApi
  class PressAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :customer do
    namespace :press do
      resource :list do
        desc 'Gets the list of presses on Peipa.'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          optional :gp,       type: String, desc: "Global position of customer"
          optional :page,     type: String, desc: "Page of press"
        end

        get do
          return Peipa::PressLib::Engine.process(service: "list",
                                                    params: params)
        end
      end

      resource :profile do
        desc 'Gets the details of a printing press.'

        params do
          requires :msisdn,     type: String, desc: 'Telephone number of customer.'
          requires :country,    type: String, desc: 'Country of customer.'
          requires :press_id,   type: String, desc: "Global position of customer"
        end

        get do
          return Peipa::PressLib::Engine.process(service: "profile",
                                                    params: params)
        end
      end

      resource :register do
        desc 'Creates a new press on Peipa.'

        params do
          requires :msisdn,     type: String, desc: 'Telephone number of customer.'
          requires :country,    type: String, desc: 'Country of customer.'
          optional :lat,         type: String, desc: "Global position of customer"
          optional :lng,         type: String, desc: "Global position of customer"
          requires :name,       type: String, desc: "Name of company"
          requires :telephone,  type: String, desc: "Telephone of company"
          optional :location_address, type: String, desc: "Location of the customer"
          optional :profile_pic_url, type: String, desc: "Profile picture of user: from s3"
          optional :location_pic_url, type: String, desc: "Location picture of user: from s3"
        end

        post do
          return Peipa::PressLib::Engine.process(service: "register",
                                                    params: params)
        end
      end

      resource :services do
        desc 'Creates a new press on Peipa.'

        params do
          requires :msisdn,     type: String, desc: 'Telephone number of customer.'
          requires :country,    type: String, desc: 'Country of customer.'
          requires :press_id,   type: String, desc: "Global position of customer"
          requires :name,       type: String, desc: "Name of service"
        end

        post do
          return Peipa::PressLib::Engine.process(service: "service|registration",
                                                    params: params)
        end
      end
    end
    end
  end
end
