module Peipa::CustomerApi
  class CloudAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :customer do
      namespace :cloud do
        resource :files do
          desc 'Creates a new user on Peipa.'

          params do
            requires :msisdn,   type: String, desc: 'Telephone number of customer.'
            requires :country,  type: String, desc: 'Country of customer.'
            optional :page,     type: String, desc: 'Pagination of file list'
          end

          get do
            return Peipa::CloudLib::Engine.process(service: "list",
                                                   params: params)
          end
        end
      end
    end
  end
end
