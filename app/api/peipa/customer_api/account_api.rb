module Peipa::CustomerApi
  class AccountAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :customer do
      namespace :account do
        resource :history do
          desc 'Creates a new user on Peipa.'

          params do
            requires :msisdn, type: String, desc: 'Telephone number of customer.'
            requires :country, type: String, desc: 'Country of customer.'
          end

          get do
            return Peipa::AccountLib::Engine.process(service: "history",
                                                     params: params)
          end
        end

        resource :payment do
          desc 'Creates a new user on Peipa.'

          params do
            requires :msisdn, type: String, desc: 'Telephone number of customer.'
            requires :country, type: String, desc: "Country of customer."
            requires :amount, type: String, desc: 'Amount customer wants to pay'
            requires :paymentMsisdn, type: String, desc: "Wallet to be debited"
          end

          post do
            # Set parameters
            options = {params: {msisdn: params[:msisdn], country: params[:country],
                                amount: params[:amount], paymentMsisdn: params[:paymentMsisdn],
                                voucher: params[:voucher], carrier: params[:carrier]}}

            # post work in worker
            # Resque.enqueue(MobileMoneyWorker, options)
            MobileMoneyWorker.perform_async(options)

            # Return this message to the customer
            return {success: true, message: "Your payment request is being proccessed. Thank you."}
          end
        end

        resource :callback do
          desc 'Creates a new user on Peipa.'

          params do
          end

          post do
            # Standard Kratos response
            # ------------------------------------------------------
            # mobile
            # currency
            # amount
            # reference - PeipaReference
            # providerReference - Telco Reference / MTN / Tigo and Airtel
            # remarks
            # status - success / failed / insufficient_funds
            
            # If response is not empty
            # save response in a telcos_response.log file
            TELCO_RESPONSES.info("#{params.to_json}")
            
            # # Check the ip address of the person posting the request
            # unless request.env['REMOTE_ADDR'] == ENV["callback_ip"]
            #   return {success: false, message: "Unauthorized access"}
            # end
            
            #If request contains no body return this message
            if params.empty?
              return {success: true, message: "Hello.. Welcome to Peipa"}
            end

            # Filter the IP Address
            TELCO_RESPONSES.info("IP Address Making Request: #{request.env['REMOTE_ADDR']}")

            return Peipa::AccountLib::Engine.process(service: "callback",
                                                     params: params)
          end
        end
      end
    end
  end
end
