module Peipa::CustomerApi
  class CustomerAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :customer do
    namespace :customers do
      resource :signup do
        desc 'Creates a new user on Peipa.'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :imei,     type: String, desc: 'IMEI'
        end

        post do
          return Peipa::CustomerLib::Engine.process(service: "registration",
                                                    params: params)
        end
      end

      # Verifying of customer with verification code
      resource :verify do
        desc 'Verify customer with code'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :code,     type: String, desc: 'Verification code'
        end

        post do
          return Peipa::CustomerLib::Engine.process(service: "verification",
                                                    params: params)
        end
      end


      # Authenticate customer
      resource :auth do
        desc 'Authenticate the customer'
        params do
          requires :msisdn,       type: String, desc: 'Telephone number of customer.'
          requires :country,      type: String, desc: "Country of customer"
          requires :pin_code,     type: String, desc: 'Verification code'
        end

        post do
          return Peipa::CustomerLib::Engine.process(service: "authentication",
                                                    params: params)
        end
      end

      resource :change_pin do
        desc 'Sets customer authentication code'
        params do
          requires :msisdn,       type: String, desc: 'Telephone number of customer.'
          requires :country,      type: String, desc: "Country of customer"
          requires :pin_code,     type: String, desc: 'Verification code'
          optional :current_pin,  type: String, desc: 'Current pin of customer'
        end

        post do
          return Peipa::CustomerLib::Engine.process(service: "pin_change",
                                                    params: params)
        end
      end


      # Logout user
      resource :logout do
        desc 'Returns the list of orders made by customer'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer'
        end

        post do
          return Peipa::CustomerLib::Engine.process(service: "logout",
                                                    params: params)
        end
      end


      # Display customer profile
      resource :profile do
        desc 'Returns the profile of customer'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer'
        end

        get do
          return Peipa::CustomerLib::Engine.process(service: "profile",
                                                    params: params)
        end

        desc 'Changes customer profile'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer'
          optional :ppt_pic,  type: String, desc: "Passport picture of customer"
          optional :full_name, type: String, desc: "Full Name of customer" 
          optional :location_address,  type: String, desc: "Location address of customer"
          optional :email,    type: String, desc: "Email of customer"
        end

        put do
          return Peipa::CustomerLib::Engine.process(service: "update_profile",
                                                    params: params)
        end
      end

      # Show list of orders for customer
      resource :orders do
        desc 'Returns the list of orders made by customer'

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer'
        end

        get do
          return Peipa::OrderLib::Engine.process(service: "customer_list",
                                                    params: params)
        end
      end
    end
    end
  end
end
