module Peipa::CustomerApi
  class ServiceAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :customer do
    namespace :services do
      resource :create do
        desc 'Creates a new order for customer.'

        params do
          requires :msisdn,        type: String, desc: 'Telephone number of customer.'
          requires :country,       type: String, desc: 'Country of customer.'
          requires :name,          type: String, desc: 'Document type'
          requires :description,   type: String, desc: 'Paper type being used'
        end

        post do
          return Peipa::PressLib::Engine.process(service: "service|registration",
                                                    params: params)
        end
      end
      
      resource :list do      
        desc "Provides the list of orders"

        params do
          requires :msisdn,   type: String, desc: 'Telephone number of customer.'
          requires :country,  type: String, desc: 'Country of customer.'
          requires :user,     type: String, desc: 'Selects the user either Customer/Press'
        end

        get do
          return Peipa::OrderLib::Engine.process(service: "list",
                                                    params: params)
        end
      end
    end
    end
  end
end
