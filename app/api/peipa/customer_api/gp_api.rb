# Updates the gps locations for devices
# For Press, Runner and customers
module Peipa::CustomerApi
  class GpAPI < Grape::API
    helpers do
      private
    end

    #receives callback url for cashin
    # https:// base_url/api/v1/signup.json
    namespace :customer do
    namespace :gps do
      resource :location do
        desc 'Creates a new order for customer.'

        params do
          requires :msisdn,     type: String, desc: 'Telephone number of customer.'
          requires :country,    type: String, desc: 'Country of customer.'
          optional :device_id,  type: String, desc: "Device ID being used"
          requires :lat,        type: String, desc: "Latitude of GPS"
          requires :lng,        type: String, desc: "Longitude of GPS"
        end

        post do
          return Peipa::GpLib::Engine.process(service: "register",
                                                    params: params)
        end


        desc "Provides the location of customer / device using msisdn / device id"

        params do
          requires :msisdn,     type: String, desc: 'Telephone number of customer.'
          requires :country,    type: String, desc: 'Country of customer.'
          optional :device_id,  type: String, desc: "Device ID being used"
        end

        get do
          return Peipa::GpLib::Engine.process(service: "retrieve",
                                                    params: params)
        end
      end
    end
    end
  end
end
