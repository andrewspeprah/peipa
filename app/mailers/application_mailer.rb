class ApplicationMailer < ActionMailer::Base
  default from: 'peipapro@yandex.ru'
  layout 'mailer'
end
