class WithdrawalMailer < ApplicationMailer
  default from: "peipapro@gmail.com"

  def withdrawal_mail(subject, message)
    cfo_email = "peipapro@gmail.com"
    @message = message
    mail(to: cfo_email, subject: subject)
  end
end
