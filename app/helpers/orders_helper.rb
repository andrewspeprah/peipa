module OrdersHelper
  def color_format(color)
    if color.downcase.include? "col"
      "<span class='badge badge-danger'>#{color}</span>".html_safe
    else
      color
    end
  end
end
