# Sends withdrawal email to CFO to
# initiate a payment
class WithdrawalEmailWorker
  include Sidekiq::Worker

  def perform(options)
    WithdrawalMailer.withdrawal_mail(options[:subject], options[:message])
  end
end
