# Airtime Worker queues airtime requests and sends them to the telcos server
# Params 	- country_id
# 				- user_id
# 				-	user_type
#  				-	message
class NotifyWorker
  include Sidekiq::Worker
  sidekiq_options retry: false
  
  def perform(options)
    options = options.symbolize_keys

    country = Country.find_by_id(options[:country_id])
    return false unless country.present?

    user = 
	    case options[:user_type]
	    when "customer"
	    	country.customers.find_by_id(options[:user_id])
	    when "press"
	    	country.presses.find_by_id(options[:user_id])
	    when "runner"
	    	country.runners.find_by_id(options[:user_id])
	    end

	  return false unless user.present?

	  Peipa::FcmLib::Notification.process(user: user, message: options[:message], collapse_key: options[:collapse_key])
  end
end
