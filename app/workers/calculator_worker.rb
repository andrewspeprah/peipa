# Airtime Worker queues airtime requests and sends them to the telcos server
class CalculatorWorker
  include Sidekiq::Worker
  
  def perform(options)
    options = options.symbolize_keys
    options[:order] = Order.find_by_id(options[:order_id])
    options[:params] = options[:params].symbolize_keys

    puts "Order not found" unless options[:order].present?
    return {success: false, message: "Order not found"} unless options[:order].present?

    Peipa::OrderLib::OrderEstimate.process(options)
    # Get order uid
    options[:params][:order_uid] = options[:order].uid
    options[:params][:state] = "confirmed"
    options[:service] = "status"

    # auto confirm order for customer
    Peipa::OrderLib::Engine.process(options)
  end
end
