# Airtime Worker queues airtime requests and sends them to the telcos server
class MetadataWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(options)
    options = options.symbolize_keys
    options[:service] = "metadata"
    options[:params] = options[:params].symbolize_keys

    Rails.logger.info options
    Rails.logger.info Peipa::FilesLib::Engine.process(options)
  end
end
