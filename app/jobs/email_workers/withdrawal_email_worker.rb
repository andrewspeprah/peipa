# Sends withdrawal email to CFO to
# initiate a payment
class EmailWorkers::WithdrawalEmailWorker
  def self.queue
    :estimator
  end

  def self.perform(options)
    WithdrawalMailer.withdrawal_mail(options[:subject],options[:message])
  end
end
