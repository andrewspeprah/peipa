# Airtime Worker queues airtime requests and sends them to the telcos server
class PaymentWorkers::MobileMoneyWorker
  # include Sidekiq::Worker
  # sidekiq_options retry: false
  def self.queue
    :estimator
  end

  def self.perform(options)
    # Symbolize keys of string params
    options = options.symbolize_keys
    options[:params] = options[:params].symbolize_keys
    options[:service] = "payment"

    # Process payment in engine library
    Peipa::AccountLib::Engine.process(options)
  end
end
