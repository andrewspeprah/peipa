class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :role

  # the user association
  has_one :press_user
  has_one :press, through: :press_user

  def authenticate(password)
    self.valid_password?(password) ? self : nil
  end

  def has_press?
    self.press.present?
  end
end
