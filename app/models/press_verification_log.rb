class PressVerificationLog < ApplicationRecord
	belongs_to :press
	belongs_to :verification_log
end
