class Service < ApplicationRecord
	belongs_to :press
	has_many 	 :papers, dependent: :destroy

	validates :name, presence: true, uniqueness: { scope: :press_id }
end
