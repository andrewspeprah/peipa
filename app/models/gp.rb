class Gp < ApplicationRecord
	has_one :customer_gp
	has_one :customer, through: :customer_gp

	has_one :runner_gp
	has_one :runner, through: :runner_gp

	has_one :press_gp
	has_one :press, through: :press_gp

	def update_location(options = {})
		self.update_attributes(options)
	end
end
