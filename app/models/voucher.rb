class Voucher < ApplicationRecord
	belongs_to :country
	validates :number, presence: true, uniqueness: { scope: :country_id }

	money_field :amount

	scope :unexpired, -> { where("expires_at > ?", Time.now ) }

	def used!
		self.update_attribute(:is_used, true)
	end

	def used?
		self.is_used
	end

	def self.unprinted
		self.where(is_printed: false)
	end

	# It indicates that voucher has been printed
	def printed!
		self.update_attribute(:is_printed, true)
	end
end
