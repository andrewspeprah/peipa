class AccountHistory < ApplicationRecord
	belongs_to :account

	money_fields :amount,:new_balance

	def customer
		self.account.try(:customer)
	end

	# Service in nice way
	def pretty_service
		case self.service
		when "ecash_deposit"
			"Deposit - ECash"
		when "ecash_withdrawal"
			"Withdrawal - ECash"
		end
	end

	# Generate csv
	def self.generate_csv(transactions)
		require 'csv'

    CSV.generate(headers: true) do |csv|
	  # Add header
	  # Amount	Service	Remarks	Transaction Type	Date	Time
      header = [
      	"NO",
        "AMOUNT",
        "SERVICE",
        "REMARKS",
        "TRANSACTION TYPE",
        "DATE",
        "TIME"
      ]
      csv << header

      # Add rows
      cnt = 0
      transactions.find_each do |transaction|
      	cnt += 1
        csv << [
        	cnt,
          transaction.amount.to_f,
          transaction.service.titleize,
          transaction.remarks,
          transaction.transaction_type,
          transaction.created_at.strftime("%d-%b-%Y"),
          transaction.created_at.strftime("%H:%M:%S")
        ]
      end
    end
	end
end
