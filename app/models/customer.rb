class Customer < ApplicationRecord
  has_secure_password

  validates :msisdn, phone: true
  validates :msisdn, presence: true, uniqueness: {scope: :country}

  # Device identification and uniqueness
  has_one :customer_device, dependent: :destroy
  has_one :device, through: :customer_device, dependent: :destroy

  # GPS location
  has_one :customer_gp, dependent: :destroy
  has_one :gp, through: :customer_gp, dependent: :destroy

  has_many :customer_verification_logs, dependent: :destroy
  has_many :verification_logs, through: :customer_verification_logs, dependent: :destroy

  # Account for customer
  has_one :customer_account, dependent: :destroy
  has_one :account, through: :customer_account, dependent: :destroy

  has_one :customer_profile, dependent: :destroy
  has_one :profile, through: :customer_profile

  has_one :country_customer
  has_one :country, through: :country_customer

  # Orders
  has_many :orders, dependent: :destroy
  has_many :documents, dependent: :destroy

  # Deliveries
  has_many :deliveries

  # Transactions - Mobile Money / VISA
  has_many :transactions

  # Account
  def account
    if super.present?
      super
    else
      self.account = Account.create
    end
  end

  def unverify!
    self.update_attribute(:is_verified, false)
  end

  def gp
    super || self.gp = Gp.new(lat: 0, lng: 0)
  end

  # Profile building
  def profile
    super || build_profile.save ? super : build_profile
  end

  # Device building
  def device
    if super.present?
      super
    else
      self.device = Device.create
    end
  end

  # Logout suer
  def logout!
    self.device.disable!
  end
end
