class VerificationLog < ApplicationRecord
	has_one :customer
	has_one :press

	def verified!
		self.update_attribute(:is_verified, true)
	end
end
