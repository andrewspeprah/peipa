class Device < ApplicationRecord
	has_one :customer_device
	has_one :customer, through: :customer_device

	has_one :runner_device
	has_one :runner, through: :runner_device

	has_one :press_device
	has_one :press, through: :press_device

	# Disable device
	def disable!
		self.update_attribute(:is_active, false)
	end

	# Enable device
	def enable!
		self.update_attribute(:is_active, true)
	end
end
