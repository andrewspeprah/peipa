class Order < ApplicationRecord
  has_many :order_documents
  has_many :documents, through: :order_documents

  belongs_to :press
  belongs_to :customer

  has_many :order_extracharges, dependent: :destroy
  has_many :extra_charges, through: :order_extracharges

  # Country from which order comes from
  has_one :country

  # Order has many documents
  has_many :order_documents, dependent: :destroy
  has_many :documents, through: :order_documents, dependent: :destroy

  # Delivery information
  has_one :delivery, dependent: :destroy

  # Comments on order
  has_many :order_comments, dependent: :destroy
  has_many :comments, through: :order_comments

  # Collection logs
  # thus how the order is exchanged
  # MAX 2
  has_many :collection_logs, dependent: :destroy

  # Charges
  has_one :charge_log, dependent: :destroy

  # Money fields
  money_fields :amount, :copies_amount

  # Calculation of order
  def complete!(is_complete = true)
    self.update_attributes(is_complete: is_complete, is_viewed: true)
  end

  # State of order
  def order_complete!
    self.update_attributes(state: "complete", is_viewed: true)
  end

  # Provides the geolocation of document
  def get_geolocation
    case self.status
    when "pending_confirmation", "pending_pickup"
      {lat: "", lng: ""}
    when "in_delivery"
      {lat: "", lng: ""}
    when "complete", "cancelled"
      customer_gp = self.customer.gp
      {lat: customer_gp.lat, lng: customer_gp.lng}
    end
  end

  # Updates invoice amount
  def update_amount(params)
    self.update_attributes(amount: params[:grand_total],
                           is_estimated: true,
                           state: "pending_confirmation",
                           copies_amount: params[:copies_amount],
                           no_of_sheets: params[:no_of_sheets])
  end

  # Get any additional cost
  # of customer
  def additional_cost
    Money.new(self.extra_charges.sum(:amount_in_pesewas), self.currency)
  end

  # Get the extra charges in a string
  def string_extra_charges
    self.extra_charges.map { |charge| "#{charge.name.titleize}:  #{charge.currency}#{charge.amount.to_f}" }.join("\n")
  end

  # Builds delivery
  def delivery
    super || build_delivery.save ? super : build_delivery
  end

  # Reviewed order
  def reviewed!
    self.update_attributes(state: "reviewed")
  end

  # Reject order
  def rejected!(message = nil, from = "")
    self.update_attribute(state: "rejected", is_viewed: true)

    if message.present?
      comment = Comment.new(message: message, from: from)
      if comment.save
        self.comments << comment
      end
    end
  end

  # Check whether order is rejected
  def rejected?
    self.state == "rejected"
  end

  def confirmed?
    self.state == "confirmed"
  end

  def pending?
    self.state == "pending_confirmation"
  end

  # Archives an order
  def archive!(user, reverse = false)
    update_params = {}

    # Get current flags and split
    current_flags = self.archive_flags.split(",")
    flag = reverse ? "0" : "1"

    case user
    when "customer"
      current_flags[0] = flag
    when "press"
      current_flags[1] = flag
    when "runner"
      current_flags[2] = flag
    end

    # If is archived by customer
    # then the whole order is being removed
    # order can only be archived by customer
    # if it is not confirmed or it is completed
    update_params.merge!({archive_flags: current_flags.join(",")})
    if user == "customer"
      flag = !reverse
      update_params.merge!({is_archived: flag})
    end

    # Update the archive state
    self.update_attributes(update_params)
  end

  # checks whether an order has been
  # deleted
  def archived?(user, reverse = true)
    # Get current flags and split
    current_flags = self.archive_flags.split(",")
    flag = reverse ? "1" : "0"

    case user
    when "customer"
      return current_flags[0] == flag
    when "press"
      return current_flags[1] == flag
    when "runner"
      return current_flags[2] == flag
    else
      return true
    end
  end

  # Unarchive transaction
  def unarchive!(user)
    archive!(user, true)
  end

  # Unarchive transaction
  def confirmed!
    self.update_attributes(state: "confirmed", is_invoice: false)
  end

  # Unarchive transaction
  def delivered!
    self.update_attributes(state: "delivered")
  end

  # Amount minus the fees
  def amount_without_fees
    self.amount - self.extra_charges.find_by_name("print_fees").amount
  end
end
