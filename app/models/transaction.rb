class Transaction < ApplicationRecord
  paginates_per 15

  # Make sure provider reference is around
  validates :provider_reference, :uniqueness => true, :presence => true
  belongs_to :customer

  # Saves transaction into db according to provider
  def self.save_transaction(options)
    # Select most recent provider transaction
    recent_provider_trans = most_recent_trans

    # if there is a recent provider trans
    # set the provider current balance
    if recent_provider_trans
      current_balance = recent_provider_trans.current_balance
    else
      current_balance = 0.0
    end

    # Check for new balance
    # if transaction failed new balance wouldn't be increased
    # or decreased
    # TODO: make this dynamic and configurable
    if options[:status] == "Paid"
      case options[:service]
      when "ecash_deposit"
        current_balance += options[:amount].to_f
      when "ecash_withdrawal"
        current_balance -= options[:amount].to_f
      end
    end

    # Save transaction into db
    #<Transaction id: nil, amount_in_pesewas: 0, current_balance_in_pesewas: 0,
    # provider: nil, status: nil, msisdn: nil, reference: nil, provider_reference: nil,
    # service: nil, params: nil, created_at: nil, updated_at: nil>
    transaction = Transaction.new(amount_in_pesewas: (options[:amount] * 100),
                                  current_balance_in_pesewas: (current_balance * 100),
                                  provider: options[:provider],
                                  status: options[:status],
                                  msisdn: options[:msisdn],
                                  reference: options[:reference],
                                  provider_reference: options[:provider_reference],
                                  service: options[:service],
                                  params: nil,
                                  remarks: options[:remarks])

    # Log transaction incase it doesn't save
    # Add the error to it as well
    unless transaction.save
      ERROR_TRANSACTION.info("Transaction: -- #{transaction.as_json}")
      ERROR_TRANSACTION.info("Error Associated: -- #{transaction.errors.as_json}")
    end
  end

  # current balance of transaction
  def current_balance
    self.current_balance_in_pesewas / 100.0
  end

  # amount
  def amount
    self.amount_in_pesewas / 100.0
  end

  # Get the most recent trans
  def self.most_recent_trans(options = {})
    if options.present?
      self.where(provider: options[:provider]).order("created_at DESC").first
    else
      self.order("created_at DESC").first
    end
  end

  # Current balance of transactions
  def self.current_balance(provider = "")
    if provider.present?
      self.where(provider: provider).order("created_at DESC").first.current_balance
    else
      # get current balances of all providers
      # and add together
      total_balance = 0
      ["mtn", "airtel", "tigo", "vodafone"].each do |provider|
        total_balance += current_balance(provider)
      end
      return total_balance
    end
  end

  # Today's transactions
  def self.today_transactions(provider = "")
    if provider.present?
      self.where(created_at: Time.now.beginning_of_day..Time.now.end_of_day,
                 provider: provider).order("created_at DESC").sum(:amount_in_pesewas) / 100.0
    else
      self.where(created_at: Time.now.beginning_of_day..Time.now.end_of_day).order("created_at DESC").sum(:amount_in_pesewas) / 100.0
    end
  end

  # Service in nice way
  def pretty_service
    case self.service
    when "ecash_deposit"
      "Deposit - ECash"
    when "ecash_withdrawal"
      "Withdrawal - ECash"
    end
  end

  # Generate csv
  def self.generate_csv(transactions)
    require 'csv'

    CSV.generate(headers: true) do |csv|
      # Add header
      header = [
        "NO",
        "PROVIDER REFERENCE",
        "TELEPHONE",
        "PROVIDER",
        "AMOUNT",
        "SERVICE",
        "STATUS",
        "TIMESTAMP",
      ]
      csv << header

      # Add rows
      cnt = 0
      transactions.find_each do |transaction|
        cnt += 1
        csv << [
          cnt,
          transaction.provider_reference,
          transaction.msisdn,
          transaction.provider,
          transaction.amount,
          transaction.pretty_service,
          transaction.status,
          transaction.created_at.to_s,
        ]
      end
    end
  end
end
