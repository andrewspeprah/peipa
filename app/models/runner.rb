class Runner < ApplicationRecord
	has_secure_password

	has_one :runner_device, dependent: :destroy
	has_one :device, through: :runner_device, dependent: :destroy

	has_one :runner_gp, dependent: :destroy
	has_one :gp, through: :runner_gp, dependent: :destroy

	# ACCOUNT
	has_one :runner_account, dependent: :destroy
	has_one :account, through: :runner_account, dependent: :destroy

	belongs_to :country

	# PRINTING PRESS
	has_one :press

	# DELIVERIES
	has_many :deliveries

	# VALIDATE PHONE NUMBER
	validates :msisdn, phone: true
	validates :msisdn, presence: true, uniqueness: { scope: :country }

	def gp
		super || self.gp = Gp.new(lat: 0, lng: 0)
	end

	# Account
	def account
		if super.present?
			super
		else
			self.account = Account.create
		end
	end

	# Device building
	def device
		if super.present?
			super
		else
			self.device = Device.create
		end
	end
end
