class Comment < ApplicationRecord

	has_one :order_comment
	has_one :order, through: :order_comment
end
