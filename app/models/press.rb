class Press < ApplicationRecord
	has_one :press_device
	has_one :device, through: :press_device

	has_one :press_gp
	has_one :gp, through: :press_gp

	has_one :country_press, dependent: :destroy
	has_one :country, through: :country_press

	validates :msisdn, phone: true
	validates :msisdn, presence: true, uniqueness: { scope: :country }

	has_many :services, dependent: :destroy

	has_many :deliveries

	# VERIFICATION CODES
	has_many :press_verification_logs, dependent: :destroy
	has_many :verification_logs, through: :press_verification_logs, dependent: :destroy

	# ORDERS
	has_many :orders

	# ACCOUNT
	has_one :press_account, dependent: :destroy
	has_one :account, through: :press_account, dependent: :destroy

	# Has many users
	has_many :press_users, dependent: :destroy
	has_many :users, through: :press_users

	# Runners for the press
	has_many :press_runners, dependent: :destroy
	has_many :runners, through: :press_runners

	def gp
		super || self.gp = Gp.new(lat: 0, lng: 0)
	end

	# Approve a press
	def approve!
		self.update_attribute(:is_approved, true)
	end

	# Verify press
	def verified!
		self.update_attribute(:is_verified, true)
	end

	# Account building
	# Account
	def account
		if super.present?
			super
		else
			self.account = Account.create
		end
	end

	# Device building
	def device
		if super.present?
			super
		else
			self.device = Device.create
		end
	end

	# Scope search
	def self.available
		self.where("is_verified = true AND is_approved = true AND is_active = true")
	end
end
