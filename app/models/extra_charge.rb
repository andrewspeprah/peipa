class ExtraCharge < ApplicationRecord
	has_one :order_extracharge
	has_one :order, through: :order_extracharge

	money_fields :amount
end
