class PressUser < ApplicationRecord
  belongs_to :press
  belongs_to :user
end
