# All pictures
# and documents are regarded as documents
class Document < ApplicationRecord
	has_many :order_documents
	has_many :orders, through: :order_documents
	has_one  :metadatum, dependent: :destroy
	belongs_to  :customer

	def metadatum
		super || build_metadatum.save ? super : build_metadatum
	end

	def etag
		self.md5_hash
	end
end
