class ChargeLog < ApplicationRecord
	belongs_to :order

	money_fields :gross_amount, :charged_ammount
end
