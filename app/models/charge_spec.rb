class ChargeSpec < ApplicationRecord
	money_fields :amount, :fee

	belongs_to :country

	before_save :set_currency

	# Set currency
	def set_currency
		self.currency = self.country.currency.code
	end
end
