class Product < ApplicationRecord
    validates :name, presence: true, uniqueness: {scope: :code}

    has_many :product_logs, dependent: :destroy
end
