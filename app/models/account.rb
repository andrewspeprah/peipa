class Account < ApplicationRecord
  has_many :account_histories

  money_fields :balance

  # toping up account with voucher
  def topup(voucher)
    # if voucher is already used send false
    if voucher.used?
      return {success: false, message: "Voucher has already been used by another customer."}
    end

    # TODO: Check rate
    new_balance = credit!(amount: voucher.amount, service: "voucher_topup", remarks: "Voucher topup with #{voucher.number}")

    if new_balance
      # update voucher and make it used
      voucher.used!
      return {success: true, message: "Account successfully topped up", data: {currency: voucher.amount.currency.code, amount: voucher.amount.to_f}}
    else
      return {success: false, message: "Account toppup failed. Please try again."}
    end
  end

  # Credits the account
  # and create an account history
  # to keep track of customers transactions
  # params 	- amount
  # 			-	service
  # 			-	remarks
  def credit!(options)
    new_balance = self.balance + options[:amount]

    if self.update_attribute(:balance, new_balance)
      # Log account history
      self.account_histories.create(service: options[:service],
                                    remarks: options[:remarks],
                                    new_balance: new_balance,
                                    amount: options[:amount],
                                    transaction_type: "Credit")
      return new_balance
    end

    return false
  end

  # Debits the account
  # and create an account history
  # to keep track of customers transactions
  def debit!(options)
    new_balance = self.balance - options[:amount]

    if self.update_attribute(:balance, new_balance)
      # Log account history
      self.account_histories.create(service: options[:service],
                                    remarks: options[:remarks],
                                    new_balance: new_balance,
                                    amount: options[:amount],
                                    transaction_type: "Debit")
      return new_balance
    end

    return false
  end
end
