class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.exclude(fields)
		self.select(self.attribute_names - fields)
	end
end
