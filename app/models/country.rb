class Country < ApplicationRecord
	paginates_per 15
	
	has_many :vouchers
	has_many :country_presses
	has_many :presses, through: :country_presses

	has_many :country_orders
	has_many :orders, through: :country_orders

	has_many :country_customers
	has_many :customers, through: :country_customers

	has_many :country_runners
	has_many :runners, through: :country_runners

	has_many :charge_specs

	# either 233 (Ghana) or 1 (United States)
	def code
		country_object = ISO3166::Country.find_country_by_name(self.name)
	  	country_code = country_object.try(:country_code) || "N/A"
	end

	def next_order_id
		self.code + (sprintf '%04d', ((self.orders.last.try(:id) || 0) + 1))
	end

	def last_order_id
		self.code + (sprintf '%04d', self.orders.last.try(:id) || 0)
	end

	# Available countries
	def self.available
		self.where(is_active: true)
	end

	# Gets currency of country
	def currency
		country_object = ISO3166::Country.find_country_by_name(self.name)
		country_object.currency
	end

end
