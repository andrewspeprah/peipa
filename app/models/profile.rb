class Profile < ApplicationRecord

	has_many :company_profiles
	has_many :companies, through: :company_profiles

	has_many :customer_profiles
	has_many :customers, through: :customer_profiles
end
