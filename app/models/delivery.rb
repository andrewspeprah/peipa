class Delivery < ApplicationRecord
  belongs_to :customer
  belongs_to :runner, optional: true
  belongs_to :press, optional: true

  # Scope definition
  scope :pending, -> { where(status: "pending_collection") }
end
